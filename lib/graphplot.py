#
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#
#    This file is part of USN.
#
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#
#

import pickle
import matplotlib.pyplot as plt
import igraph
import matplotlib.lines as mlines
import seaborn as sns
import csv
import os
from scipy.spatial.distance import euclidean
import numpy as np
import errno
import collections
import pandas as pd

DATA_DIR = "sb_output"
POPULATION_TABLE = "/populationTable.png"
DEG_DIST_WHOLE_GRAPH = "/degDistWholeGraph_{}.png"
DEG_DIST_SUB_GRAPH = "/degDistSubGraph_{}.png"
GRAPH_SB_PICKLE = DATA_DIR+"/sb_graph.pickle"
POP_INFO = DATA_DIR+"/pop_info.pickle"
POPULATION = DATA_DIR+"/population.pickle"
GLOBAL_METRICS_TABLE_PNG = "/globalMetrics.png"
GLOBAL_METRICS_TABLE_CSV = "/globalMetrics.csv"
DISTANCE_DIST_SUB_GRAPH = "/distanceDistSubGraph_{}.png"
WHOLE_GRAPH="WholeGraph"
SUB_GRAPH="SubGraph"


def global_efficiency(graph, pathlengths):
    sp = np.asarray(pathlengths)
    nozero_sp = sp[sp != 0]
    tmp = 1.0 / nozero_sp
    s = tmp.sum()
    N = graph.vcount()
    ge = s/(N*(N-1))
    return ge


def plot_deg_dist_sub_graph(graphs, dump, output_dir):
    """ ...

        Args: 
            graphs (:obj:`dict`): a dictionary containing a Graph object for each key.
            ::
                graphs = { <str:GroupName_1>:  <igraph.Graph:g_1>,
                           <str:GroupName_2>:  <igraph.Graph:g_2>,
                                ...,
                              }

        Returns: 
            ... 
    
    """  
    degree_lists = {}
    for k,v in graphs.items():
        degree_lists[k] = v.degree()
    plot_deg_dist(degree_lists, output_dir+DEG_DIST_SUB_GRAPH, dump, SUB_GRAPH, output_dir)


def plot_deg_dist_whole_graph(groups_size, sb_graph, dump, output_dir):
    """ ...

        Args: 
            groups_size (:obj:`dict`): a dictionary containing groups information.
            ::
                groups_size = { <str:GroupName_1>:  <int:size_1>,
                                <str:GroupName_2>:  <int:size_2>,
                                ...,
                              }
            sb_graph (:obj:'igraph.Graph'): a Graph object 

        Returns: 
            ... 
    
    """    
    degree_lists = {}
    acc = 0
    for k,v in groups_size.items():
        degree_lists[k] = sb_graph.degree( [i for i in range( acc, acc+v)] )
        acc +=v
    degree_lists["all groups"] = sb_graph.degree()
    plot_deg_dist(degree_lists, output_dir+DEG_DIST_WHOLE_GRAPH, dump, WHOLE_GRAPH, output_dir)
    

def plot_deg_dist(degree_lists, file_name, dump, graphName, output_dir):
    """ ...

        Args: 
            degree_lists (:obj:`dict`): a dictionary containing a list for each key.
            ::
                degree_lists = { <str:GroupName_1>:  <list:deg_1>,
                                 <str:GroupName_2>:  <list:deg_2>,
                                ...,
                               }
            file_name (:obj:`str`): the name of the file used to write the plot.

        Returns: 
            ... 
    
    """ 
    ### SCATTER PLOT
    fig = plt.figure(figsize=(20, 13), dpi= 200)
    n_cols = 2
    rows = (len(degree_lists.keys())+n_cols-1) // n_cols
    for i,(k,v) in enumerate(degree_lists.items()):
        plt.subplot(rows, n_cols, i+1)
        plt.scatter( range(len(v)), v, c=v, cmap="cool", vmin=min(v), vmax=max(v))
        plt.title(k.title() , fontweight='bold', fontsize='x-large')
        plt.xlabel('Nodes')
        plt.ylabel('Degree')
        if dump:
            with open(output_dir+"/DEGREE_"+graphName+"_"+k.title().replace(" ","")+".pickle", "wb") as fout:
                pickle.dump(v, fout, protocol=pickle.HIGHEST_PROTOCOL)
    plt.subplots_adjust(hspace=0.45)
    plt.savefig(file_name.format("scatter"))
    plt.close(fig)
    
    ### BOXPLOT PLOT
    fig = plt.figure(figsize=(12.8, 7.2), dpi= 200)
    data = []
    lab = []
    maxVal = []
    meanVal = []
    minVal = []
    for k,v in degree_lists.items():
        data.append(v)
        lab.append(k.title())
        meanVal.append(sum(v)/len(v))
        maxVal.append(max(v))
        minVal.append(min(v))
    size = 6
    ec = "white"
    blue_line = mlines.Line2D([0], [0], color='blue', marker='d', markersize=size, label='Max')
    green_line = mlines.Line2D([0], [0], color='green', marker='o', markersize=size, label='Mean')
    orange_line = mlines.Line2D([0], [0], color='orange', marker='D', markersize=size, label='Min')
    # whis = whiskers = 1.5 - vengono utilizzati per posizionare gli outliers
    # sopra -> Q1 - whis*(Q3-Q1)    sotto ->  Q3 + whis*(Q3-Q1)
    red_line = mlines.Line2D([0], [0], color='white', markerfacecolor='red', marker='X', markersize=10, label='Outliers\n below: Q1-1.5*(Q3-Q1)\n above: Q3+1.5*(Q3-Q1)')
    plt.legend(handles=[orange_line, blue_line, green_line, red_line])
    plt.plot(range(len(meanVal)), meanVal, color="green", linewidth=0.5, markersize=size, marker='o', markeredgecolor=ec, linestyle='solid')
    plt.plot(range(len(maxVal)), maxVal, color="blue", linewidth=0.5, markersize=size+3, marker='d', markeredgecolor=ec, linestyle='solid')
    plt.plot(range(len(minVal)), minVal, color="orange", linewidth=0.5, markersize=size+3, marker='d', markeredgecolor=ec, linestyle='solid')
    red_diamond = dict(markerfacecolor='red', marker='X', markeredgecolor=ec, markersize=size)
    plt.boxplot(data, labels=lab, positions=range(0,len(data)), flierprops=red_diamond)
    plt.ylabel('Degree')
    plt.title('Degree Distributions', fontweight='bold', fontsize='x-large')
    plt.savefig(file_name.format("boxplot"))
    plt.close(fig)
    
    ### DISTPLOT
    fig = plt.figure(figsize=(20,13), dpi= 200)
    for i,(k,v) in enumerate(degree_lists.items()):
        plt.subplot(rows, n_cols, i+1)
        if maxVal[i] > 0:
            sns.histplot( data=v, kde=True)
        plt.title(k.title() , fontweight='bold', fontsize='x-large')
        plt.xlabel('Degree')
        plt.ylabel('# Nodes')
    plt.subplots_adjust(hspace=0.45)
    plt.savefig(file_name.format("distplot"))
    plt.close(fig)
    

def plot_population_table(groups_size, output_dir):
    """ Creates an histogram of the population divided by group.

        Args: 
            groups_size (:obj:`dict`): a dictionary containing groups information.
            ::
                groups_size = { <str:GroupName_1>:  <int:size_1>,
                                <str:GroupName_2>:  <int:size_2>,
                                ...,
                              }

        Returns: 
            Creates a png file containing the plot of the histogram. 

    """    
    
    plt.figure(figsize=(12.8, 7.2), dpi= 200)
    columns = []
    # we have only 1 row
    row = []
    totPop = 0
    
    # Get columns' names and row's values
    for k,v in groups_size.items():
        columns.append(k.capitalize())
        row.append(v)
        totPop += v
    columns.append("Total")
    row.append(totPop)
    
    # Draw bar chart
    bar_width = 0.4
    plt.bar(columns, row, bar_width)
    plt.title('Population')
    
    # Draw table
    cell_text = []
    for i in range(len(row)):
        row[i] = "{} ({:.2f}%)".format(row[i],(row[i]/totPop*100))
    cell_text.append(row)
    # Produces a table wide as the axes (left=0, width=1) but below the axes (bottom=-0.3, height=0.15)
    plt.table( cellText=cell_text, colLabels=columns, loc='bottom', bbox=[0.0,-0.3,1,.15])
    plt.subplots_adjust(bottom=0.25)
    
    plt.savefig(output_dir+POPULATION_TABLE)

def plot_global_metrics(graphs,output_dir):
    """ ...

        Args: 
            graphs (:obj:`dict`): a dictionary containing a Graph object for each key.
            ::
                graphs = { <str:GroupName_1>:  <igraph.Graph:g_1>,
                           <str:GroupName_2>:  <igraph.Graph:g_2>,
                                ...,
                              }

        Returns: 
            ... 
    
    """  
    ### Global clustering coefficient
    # - The global clustering coefficient is the number of closed triplets (or 3 x triangles) 
    # - over the total number of triplets (both open and closed). The transitivity measures 
    # - the probability that two neighbors of a vertex are connected. More precisely, this is 
    # - the ratio of the triangles and connected triplets in the graph. 
    #[ transitivity_undirected() ] 
    # - The result is a single real number. Directed graphs are considered as undirected ones.
    ### Local clustering coefficient
    # - The local clustering coefficient of a vertex (node) in a graph quantifies how close its 
    # - neighbours are to being a clique (complete graph). 
    # - The local clustering coefficient C_i for a vertex v_i is then given by the proportion of 
    # - links between the vertices within its neighbourhood divided by the number of links that 
    # - could possibly exist between them.
    # [ transitivity_local_undirected() ]
    ### Network average clustering coefficient
    # - the average of the local clustering coefficients of all the vertices n
    # [ transitivity_avglocal_undirected() ]
    ### Assortativity
    # - Assortativity, or assortative mixing is a preference for a network's nodes to attach to 
    # - others that are similar in some way.
    #[ assortativity() ]
    # - This coefficient is basically the correlation between the actual connectivity patterns 
    # - of the vertices and the pattern expected from the disribution of the vertex types.
    #[ assortativity_degree() ]
    # - Returns the assortativity of a graph based on vertex degrees.
    ### Diameter
    # The longest of all the calculated shortest paths in a network
    ### Radius
    # The minimum eccentricity of its vertices
    ### Eccentricity
    # The eccentricity of a vertex is calculated by measuring the shortest distance 
    # from (or to) the vertex, to (or from) all other vertices in the graph, and taking the maximum.
    columns = []
    rowNodes = []
    rowEdges = []
    rowDensity = []
    rowAvgDeg = []
    rowGlobClust = []
    rowAvgLocClust = []
    rowAssortativityDeg = []
    rowDiameter = []
    rowRadius = []
    rowAvgSP = []
    rowGlobEff = []
    rowCC = []
    rowSizeGCC = []
    rows = ["# Nodes", "# Edges", "Density", "Average Degree", "Global Clustering \nCoefficient",
            "Average of Local \nClustering Coefficients", "Assortativity Degree",
            "Diameter", "Radius", "Average Shortest Path", "Global Efficiency", "Connected Components", "Giant Component Size"]
    cell_text = [rowNodes, rowEdges, rowDensity, rowAvgDeg, rowGlobClust, rowAvgLocClust, rowAssortativityDeg, rowDiameter, rowRadius, rowAvgSP, rowGlobEff, rowCC, rowSizeGCC]
    for group_name, group_graph in graphs.items():
        columns.append( group_name.title() )
        vcount = group_graph.vcount()
        ecount = group_graph.ecount()
        #sp = group_graph.shortest_paths()
        rowNodes.append(vcount)
        rowEdges.append(ecount)
        rowDensity.append( round( (2*ecount)/(vcount*(vcount-1)), 4) )
        rowAvgDeg.append( round(2*ecount/vcount, 2) )
        # the result will be NaN if the graph does not have any triplets (mode="nan")
        rowGlobClust.append( round(group_graph.transitivity_undirected(mode="nan"), 4) )
        # vertices with degree less than two will have zero transitivity (mode="zero")
        rowAvgLocClust.append( round(group_graph.transitivity_avglocal_undirected(mode="zero"), 4) )  
        rowAssortativityDeg.append( round(group_graph.assortativity_degree(directed=False), 4) )
        rowDiameter.append( int(group_graph.diameter(directed=False)) )
        rowRadius.append( int(group_graph.radius(mode=igraph.ALL)) )
        #rowDiameter.append( round(group_graph.diameter(directed=False), 4) ) 
        #rowRadius.append( round(group_graph.radius(), 0) ) 
        rowAvgSP.append( round(group_graph.average_path_length(), 2) )
        #rowGlobEff.append( round(global_efficiency(group_graph,sp), 3) )
        rowGlobEff.append(0)
        cc = group_graph.components(mode=igraph.WEAK)
        rowCC.append(len(cc))
        rowSizeGCC.append(len(cc.giant().vs))
         
    plt.figure(figsize=(12.8, 7.2), dpi= 200)
    # Draw table
    plt.axis("off")

    # Produces a table wide as the axes (left=0.1, width=0.9) but below the axes (bottom=0.4, height=0.25)
    plt.table( cellText=cell_text, colLabels=columns, rowLabels=rows, loc="center", bbox=[0.1, 0.1, 0.95, 0.95] )
    #plt.subplots_adjust(bottom=0.25)
    plt.savefig(output_dir+GLOBAL_METRICS_TABLE_PNG)
    
    with open(output_dir+GLOBAL_METRICS_TABLE_CSV, "w") as csvfile:
        writer = csv.writer(csvfile)
        columns.append("Metric")
        writer.writerow( columns )
        for i, row in enumerate(cell_text):
            row.append(rows[i].replace("\n",''))
            writer.writerow( row )


def plot_neighbours_distance ( graphs, dump, output_dir ):
    """ ...

        Args: 
            graphs (:obj:`dict`): a dictionary containing a Graph object for each key.
            ::
                graphs = { <str:GroupName_1>:  <igraph.Graph:g_1>,
                           <str:GroupName_2>:  <igraph.Graph:g_2>,
                                ...,
                              }

        Returns: 
            ... 
    
    """  
    length_lists = {}
    for grp_name, grp_graph in graphs.items():
        length_lists[grp_name] = grp_graph.es["distance"]
        #length_lists[grp_name] = grp_graph.es["length"]
        
    ### SCATTER PLOT
    plt.figure(figsize=(20, 13), dpi= 200)
    n_cols = 2
    rows = (len(length_lists.keys())+n_cols-1) // n_cols
    for i,(k,v) in enumerate(length_lists.items()):
        plt.subplot(rows, n_cols, i+1)
        if len(v) > 0:
            plt.scatter( range(len(v)), v, c=v, cmap="cool", vmin=min(v), vmax=max(v))
        plt.title(k.title() , fontweight='bold', fontsize='x-large')
        plt.xlabel('Edges/Pairs')
        plt.ylabel('Distance')
        if dump:
            #print(k.title()+str(v[:10]) )
            with open(output_dir+"/DISTANCE_"+k.title().replace("Whole Graph","AllGroups")+".pickle", "wb") as fout:
                pickle.dump(v, fout, protocol=pickle.HIGHEST_PROTOCOL)
    plt.subplots_adjust(hspace=0.45)
    plt.savefig(output_dir+DISTANCE_DIST_SUB_GRAPH.format("scatter"))
    
    
    ### BOXPLOT PLOT
    fig=plt.figure(figsize=(12.8, 7.2), dpi= 200)
    data = []
    lab = []
    maxVal = []
    meanVal = []
    minVal = []
    for k,v in length_lists.items():
        if len(v) > 0: 
            data.append(v)
            lab.append(k.title())
            meanVal.append(sum(v)/len(v))
            maxVal.append(max(v))
            minVal.append(min(v))
    size = 6
    ec = "white"
    blue_line = mlines.Line2D([0], [0], color='blue', marker='d', markersize=size, label='Max')
    green_line = mlines.Line2D([0], [0], color='green', marker='o', markersize=size, label='Mean')
    orange_line = mlines.Line2D([0], [0], color='orange', marker='D', markersize=size, label='Min')
    # whis = whiskers = 1.5 - vengono utilizzati per posizionare gli outliers
    # sopra -> Q1 - whis*(Q3-Q1)    sotto ->  Q3 + whis*(Q3-Q1)
    red_line = mlines.Line2D([0], [0], color='white', markerfacecolor='red', marker='X', markersize=10, label='Outliers\n below: Q1-1.5*(Q3-Q1)\n above: Q3+1.5*(Q3-Q1)')
    plt.legend(handles=[orange_line, blue_line, green_line, red_line])
    plt.plot(range(len(meanVal)), meanVal, color="green", linewidth=0.5, markersize=size, marker='o', markeredgecolor=ec, linestyle='solid')
    plt.plot(range(len(maxVal)), maxVal, color="blue", linewidth=0.5, markersize=size+3, marker='d', markeredgecolor=ec, linestyle='solid')
    plt.plot(range(len(minVal)), minVal, color="orange", linewidth=0.5, markersize=size+3, marker='d', markeredgecolor=ec, linestyle='solid')
    red_diamond = dict(markerfacecolor='red', marker='X', markeredgecolor=ec, markersize=size)
    plt.boxplot(data, labels=lab, positions=range(0,len(data)), flierprops=red_diamond)
    plt.ylabel('Distance')
    plt.title('Distance Distributions', fontweight='bold', fontsize='x-large')
    plt.savefig(output_dir+DISTANCE_DIST_SUB_GRAPH.format("boxplot"))
 
    ### DISTPLOT
    plt.figure(figsize=(20,13), dpi= 200)
    for i,(k,v) in enumerate(length_lists.items()):
        plt.subplot(rows, n_cols, i+1)
        if len(v) > 0:
            sns.histplot( data=v, kde=True)
        plt.title(k.title() , fontweight='bold', fontsize='x-large')
        plt.xlabel('Distance')
        plt.ylabel('# Edges/Pairs')
    plt.subplots_adjust(hspace=0.45)
    plt.savefig(output_dir+DISTANCE_DIST_SUB_GRAPH.format("distplot"))

    
OUTPUT_DIR = "output"
def main():
    
    output_dir = OUTPUT_DIR
    try:
        os.mkdir(output_dir)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
        pass
    dump=False 
    sb_graph = igraph.Graph.Read_Pickle(GRAPH_SB_PICKLE)   
    pop_info = pickle.load(open(POP_INFO, "rb"))    
    population = pickle.load(open(POPULATION, "rb"))  
    
    # groups_size - { <str:GroupName_1>: <int:size_1>, <str:GroupName_2>: <int:size_2>, ... }  
    groups_size = {}
    cnt_types = dict(collections.Counter(population["type"]))
    for k in cnt_types.keys():
        groups_size[ pop_info['group_names'][k] ] = cnt_types[k]
        
    plot_population_table( groups_size, output_dir )
    plot_deg_dist_whole_graph( groups_size, sb_graph, dump, output_dir  )
    
    # Create a subgraph for each group in group_size
    # { <str:GroupName_1>: graph, <str:GroupName_2>: graph  }
    graphs = {}
    for i, (group_name, _ ) in enumerate(groups_size.items()):
        graphs[group_name] = sb_graph.induced_subgraph( [j.index for j in sb_graph.vs if j['type'] == i ] )
    graphs["Whole Graph"] = sb_graph
    
    plot_deg_dist_sub_graph( graphs, dump, output_dir )
    plot_global_metrics( graphs, output_dir ) 
    plot_neighbours_distance( graphs, dump, output_dir )


if __name__ == "__main__":
    main()
    
    
    
