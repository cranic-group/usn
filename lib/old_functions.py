
def data_structure_documentation():
    """  
    This is a dummy function to track all the data structures.

    **The territory** is a box of real coordinates which defines the whole
    simulation area.  (returned by read_territory)
    ::

        TERRITORY 
        {
            'origin':(So,We), 
            'bbox':[(So,We),(So,Ea),(No,Ea),(No,We)], 
            'borders':(No,So,We,Ea), 
            'grid':(grid['W'],grid['H'])
        }


    **The population** is a dictionary with global information about the
    population in the territory: the total number and the number and type of
    groups i.e. (children, young, adults, elderly).  (returned by
    read_population)
    ::

        POPULATION
        {
            'size':N, 
            'groups': {'G1':p1, 'G2':p2, ..., 'Gn':pn}
        }


    The actual **population data** with all the coordinates of each census cell
    and the number of individuals in each cell is stored in a pandas dataframe
    with four columns:
    (returned by extract_population)
    ::

        POP_DF (my_df)
        Lat, Lon, <pop_column>, probability.


    The **coordinates** of each individual in the population are stored in
    numpy arrays: (in add_noise and other functions)
    ::

        COORDINATES: 
        (Lat, Lon, Lat, Lon, ...)


    The **sampled population** for each group is stored in a dictionary of
    coordinates: (returned by sample_groups)
    ::

        POP_DICT (geo_X)
        {
            'G1': numpy.array(Lat, Lon, Lat, Lon, ...)
            'G2': numpy.array(Lat, Lon, Lat, Lon, ...)
            ...
            'Gn': numpy.array(Lat, Lon, Lat, Lon, ...)
        }



    """


def read_territory(config_file):
    """ Reads territory info from the configuration file. 
    
        A territory is defined by two parameters: borders and grid.

        The borders parameter is a dictionary containing as fields the 4
        cardinal directions (N, S, W, E). The borders define the vertices of
        the rectangle used as a territory.  
    
        The grid parameter is ...
    
    Args: 
        config_file (:obj:`str`): path to the config file 

    
    Returns: 
        (:obj:`dict`): a dictionary containing 4 fields: origin, bbox, borders and
        grid. The origin field is a tuple containing 2 values (S,W), i.e. the
        starting vertex used to draw the rectangle. The bbox field is a list of
        tuples containing the coordinates of the 4 vertices, the first one is
        the origin. The borders field is a tuple containing 4 values (N,S,W,E).
        The grid is a tuple containing 2 values (W,H).
    
        ::

            {
              'origin':(So,We), 
              'bbox':[(So,We),(So,Ea),(No,Ea),(No,We)], 
              'borders':(No,So,We,Ea), 
              'grid':(grid['W'],grid['H'])
            }

     """

    print("read_territory: reading config file: {}".format(config_file))
    #print("reading config file: {}".format(config_file))
    config = cp.ConfigParser()
    config.read(config_file)
    borders     = eval(config.get('TERRITORY','borders'))
    grid        = eval(config.get('TERRITORY','grid'))
    No,So,We,Ea = borders['N'],borders['S'],borders['W'],borders['E']
    territory   = {'origin':(So,We), 'bbox':[(So,We),(So,Ea),(No,Ea),(No,We)], 'borders':(No,So,We,Ea), 'grid':(grid['W'],grid['H'])}
    return territory


def get_square_borders(x,y,territory):
    """ Return the N,S,W,E borders of the given square.

        Warning:
            Not used yet.

    """

    No,So,We,Ea = territory['borders']
    W,H = territory['grid']
    
    if x>=W or y>=H:
        print('get_square_borders: Error! Given (x,y) is out of ({},{}) grid'.format(W,H), file=sys.stderr)
        return 101

    w = Ea-We
    h = No-So
    lx = w/W
    ly = h/H
    So = So+y*ly
    No = So+ly
    We = We+x*lx
    Ea = We+lx
    return No,So,We,Ea

def get_square(x,y,territory):
    """ Return the square that contains the given point
    
        Warning:
            Not used yet.
    """
    No,So,We,Ea = territory['borders']
    W,H = territory['grid']

    if not We<=x<=Ea or not So<=y<=No:
        print('get_square: Error! Given (x,y) is out of ({},{},{},{}) bbox'.format(No,So,We,Ea), file=sys.stderr)
        return 101

    w = Ea-We
    h = No-So
    lx = w/W
    ly = h/H
    x_c = int((x-We)//lx)
    y_c = int((y-So)//ly)
    return x_c,y_c


def gen_population(territory, pop_info):
    """ 

        Args:

        Returns:

    """
    return 


#### READ ITALIAN POPULATION DATAFRAME, RESTRICT TO AREA UNDER CONSIDERATION ####

def read_population(config_file):
    """ Reads population info from the config file.
    
        A population is defined by two parameters: size and groups.
        
        The size parameter is a number.
        
        The groups parameter is a dictionary containing the name of the groups (the
        fields of the dictionary) and a number for each group, whose total sum must
        be 1. The number is the percentage of the population each group represents.
    
    Args: 
        config_file (:obj:`str`): path to the config file 
    
    Returns:
        (:obj:`dict`): a dictionary with 2 fields: size and groups. 
        ::

            {
                'size':N, 
                'groups':groups
            }
        
    """

    config = cp.ConfigParser()
    config.read(config_file)
    N          = eval(config.get('POPULATION', 'size'))
    groups     = eval(config.get('POPULATION', 'groups'))
    percSum = 0
    for k,v in groups.items():
        percSum += v
    if percSum != 1:
        print( "The percentages of the groups must sum to 1, now it's {}".format(percSum) , file=sys.stderr)
        sys.exit(1)
    population = {'size':N, 'groups':groups}
    return population

def read_pop_csv(config_file):
    """ Reads the file names of the csvs that store population density information.
    
        Args: 
            config_file (:obj:`str`): path to the config file 

        Returns:
            (:obj:`dict`): a dictionary with one field for each group, the
            value of each field contains the csv file name.

    """

    config = cp.ConfigParser()
    config.read(config_file)
    ####
    pop_csv    = eval(config.get('FILES', 'pop_csv'))
    return pop_csv

def extract_population(pop_csv, borders, pop_column='Population', verbose=False):
    """ Reads population density data from a csv file with 3 columns named: Lat, Lon and <pop_column>.
    
        Note:
            Selects only the rows inside the borders (N,S,W,E), i.e. S <= Lat
            <= N and W <= Lon <= E. Compute the total population and the
            population of the territory selected. 
            Compute the probability of picking a person in each cell of the
            selected territory as:
            ::

                probability = pop_at_this_point/tot_pop
            
        Returns:
            (:obj:`pandas dataframe`): a dataframe with 4 columns Lat, Lon, <pop_column> and probability.

    """
    No,So,We,Ea = borders 
    pop_df = pd.read_csv(pop_csv)
    tot_pop = pop_df[pop_column].sum()
    # get subdf
    my_df = pop_df[pop_df['Lat'].between(So,No) & pop_df['Lon'].between(We,Ea)].copy()
    # get tot population
    my_pop = my_df[pop_column].sum()
    # create probability of selecting a cell proportional to the population
    my_df['probability'] = my_df[pop_column]/my_pop
    
    if verbose:
        print('tot population:', tot_pop)
        print('the cell with the largest population in whole df is:')
        print(pop_df.iloc[pop_df['Population'].idxmax()])
        print('tot population of the considered area:', my_pop)
        print('tot probatility:', my_df['probability'].sum())
    
    return my_df


#### GET COORDINATES AND SELECT RANDOM SAMPLE OF N POINTS ####

#def sample_coords(pop_df, size):
#    '''sample a population of size points from the density given in the ‘probability’ column of pop_df
#    returns a numpy array of shape (size,2)'''
#    coords_array = pop_df[['Lon','Lat']].sample(n=size, replace=True, weights=pop_df['probability']).to_numpy()
#    return coords_array

def add_noise(coords_array, borders, tot_squares):
    """ Add noise to the given coords while guaranteeing that points remain in their tile.
    
        Note:
            points may be placed outside the borders, but they remain in their tile.
    
        Args:
            coords_array (:obj:`numpy array`): numpy array of coordinates
            borders (:obj:`tuple`): a tuple containing 4 values (N,S,W,E)
            tot_squares (:obj:`int`): the total number of square tiles in the grid

        Returns:
            (:obj:`numpy array`): the modified array of coordinates with noise.

    """
    No,So,We,Ea = borders
    w = Ea-We
    h = No-So
    #M = tot_squares
    N = coords_array.shape[0]
    #num_x = np.sqrt(M*w/h)   # number of squares on x axis
    #num_y = M/num_x          # number of squares on y axis
    #lx = w/num_x
    #ly = h/num_y
    lx = ly = np.sqrt( w*h / tot_squares )
    # I punti possono uscire da borders, ma non dal singolo square/tile/cell
    noisy_coords = coords_array + np.random.random((N,2)) * (lx,ly) - (lx/2,ly/2)
    return noisy_coords
    

def squeeze(coords, borders):
    """ Squeeze coords so that bottom border is (0,1)
    
        Args:
            coords (:obj:`numpy array`): numpy array of coordinates
            borders (:obj:`tuple`): a tuple containing 4 values (N,S,W,E)

        Returns:
            (:obj:`numpy array`): numpy array of squeezed coordinates
    
    """
    _,So,We,Ea = borders
    w = Ea-We
    X = (coords-(We,So))/(w,w)
    return X


# territory { 'origin':(So,We), 'bbox':[(So,We),(So,Ea),(No,Ea),(No,We)], 'borders':(No,So,We,Ea), 'grid':(grid['W'],grid['H']) }
# population {'size':N, 'groups':  {'children':.05, 'young':.15, 'adults':.5, 'elderly':.3}  }] 
# groups_df [DATAFRAME] di 4 colonne: Lat, Lon, Population, probability
def sample_groups(groups_df,population,territory):
    """ Sample (with noise) points for all groups and for each group, returns a
        dictionary of the sampled point (Lat, Lon) of the population.


    Args:
        groups_df (:obj:`pandas dataframe`): stores the population data per
        cell (4 columns: Lat, Lon, Population, probability) 
        population (:obj:`dict`): stores global info about population in the given territory (tot pop, groups) 
        territory (:obj:`dict`): stores territory informations.

    Returns:
        (:obj:`dict`): a dictionary indexed by the group name, each field
        stores a numpy array with all the coordinates of the points of that
        group.
        ::

            {
                'G1': numpy.array(Lat, Lon, ...)
                'G2': numpy.array(Lat, Lon, ...)
                ...
                'Gn': numpy.array(Lat, Lon, ...)
            }

    """

    N = population['size']
    groups = population['groups']
    s = 0
    geo_X = {}
    for i,(g,p) in enumerate(groups.items()):  # i,(g,p) ==> 1,('children', 0.05)
        if i == (len(groups)-1):
            Ng = N-s 
        else:
            Ng = round(p*N)
            s += Ng
        #X = sample_coords(groups_df[g], Ng)
        # Estraggo casualmente le coordinate di ogni individuo degli Ng da estrarre
        X = groups_df[g][['Lon','Lat']].sample(n=Ng, replace=True, weights=groups_df[g]['probability']).to_numpy()
        # Sposto casualmente gli individui all'interno della stessa cella, 
        # in modo che non siano tutti nello stesso punto.
        X = add_noise(X, territory['borders'], len(groups_df[g]))
        geo_X[g] = X
    return geo_X


#### SET GROUPS PROBABILITIES ####

def read_sociality(config_file):
    """ Reads sociality contacts information from config file.

        Args:
            config_file (:obj:`str`): path to the config file

        Returns:
            (:obj:`dict`): a dictionary with contact probability among different groups:  'group1': {'group2':p12} 
            ::

                {
                    'children':{'children':.2, 'young':.1, 'adults':.1, 'elderly':.1},
                    'young':   {'children':.1, 'young':.2, 'adults':.1, 'elderly':.1},
                    'adults':  {'children':.1, 'young':.1, 'adults':.2, 'elderly':.1},
                    'elderly': {'children':.1, 'young':.1, 'adults':.1, 'elderly':.2}
                }

    
    """
    config = cp.ConfigParser()
    config.read(config_file)
    ####
    g2g_probabilities = eval(config.get('SOCIALITY', 'group2group'))
    # groups = eval(config.get('POPULATION', 'groups'))
    # g2g_probabilities = np.array([[g2g_probabilities[k][h] for h in groups.keys()] for k in groups.keys()])
    return g2g_probabilities



#### SAMPLE EDGES -- MEMORY EFFICIENT ####

def edge_probability_groups(p, D, lam=1):
    '''compute the edge probabilities for fixed source group and target group'''
    # set diag to inf (no loops)
    np.fill_diagonal(D,np.inf)
    D  = 1/D
    EP = lam*p*D
    return EP

## DRAW EDGES CHUNK BY CHUNK ##

def get_edges_groups(L, s, p):
    '''sample edges between a fixed pair of groups, chunk by chunk'''
    # NB: several variables used below are global!
    # mdist, Mdist, min_d, max_d, lam
    
    # this line "normalizes" distances into a suitable range
    D = (Mdist-mdist)*((L-min_d)/(max_d-min_d))+mdist

    EP = edge_probability_groups(p, D, lam)
    
    edges = np.where(EP>np.random.random(EP.shape), 1, 0)
    
    return edges

# geo_X = { 'children': numpyArr, 'young': numpyArr, ... } -- numpyArr contiene Lat e Lon di ogni individuo
# population = {'size':N, 'groups': {'children':.05, 'young':.15, 'adults':.5, 'elderly':.3} }
# g2g_probabilities = { 'children':{'children':.2, 'young':.1, 'adults':.1, 'elderly':.1},
#                       'young':{'children':.1, 'young':.2, 'adults':.1, 'elderly':.1}, ...
def create_edge_generators(population, g2g_probabilities, geo_X):
    '''define a set of generators for edges, each only producing edges between a fixed pair of groups'''
    groups = list(population['groups'].keys())
    edge_generators = {}
    for i,g1 in enumerate(groups):
        for g2 in groups[i:]:
            p = g2g_probabilities[g1][g2]
            # reduce_func(D_chunk, start) is called repeatedly, where <D_chunk> is a contiguous vertical slice 
            # of the pairwise distance matrix, starting at row <start>. L and s in the code.
            edge_gen = pdist_chunks(geo_X[g1], geo_X[g2], reduce_func=lambda L,s:get_edges_groups(L,s,p) ) #, working_memory=1)
            edge_generators[(g1,g2)]=edge_gen
    return edge_generators


# group_size = { 'children': size, 'young': size, 'adults': size, 'elderly': size }
# edge_generators = { ('children','children')]: edge_gen, ('children','young')]: edge_gen, ... }
def create_graph(edge_generators, group_size, layer_name='BS'):
    '''create the graph based on the edge generators'''
    # G = nx.Graph()
    G = ig.Graph()
    G.add_vertices(sum(group_size.values()))
    # colors = {'SB':'black', 'CS':'gray'}
    groups = list(group_size.keys())
    # nodes_start = { 'children': SommaSizeFinoAchildren, 'young': SommaSizeFinoAyoung }
    # nodes_start = { 'children': sizeChildren, 'young': sizeChildren+sizeYoung, ... }
    nodes_start = {groups[i]:sum(group_size[h] for h in groups[:i]) for i in range(len(groups))}
    for (g1,g2),edge_gen in edge_generators.items():
        print('adding edges for groups {} and {}'.format(g1,g2))
        for i,e in enumerate(edge_gen):
            x = e.shape[0]
            es = np.column_stack(np.triu(e,i*x+1).nonzero())+i*np.array([x,0])
            es = es+[nodes_start[g1],nodes_start[g2]]
            # G.add_edges_from(es) #, layer='SB', color=colors['SB'])
            G.add_edges(es)
    G.es['layer'] = [layer_name]*G.ecount()
    return G


