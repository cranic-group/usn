import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns

sns.set_context("talk")
sns.set_style('whitegrid', rc={'figure.figsize':(17,4)})

recap_data = pd.read_pickle('epidemic_threshold_data.pickle')

d = {}
for r in recap_data.to_dict('records'):
    h = d.setdefault(r['configuration'],{})
    h[r['threshold type']] = r['threshold value']
dd = {}
for k,v in d.items():
    dd.setdefault('configuration',[]).append(k)
    for h,u in v.items():
        dd.setdefault(h,[]).append(u)
df = pd.DataFrame(dd)
my_range = range(1,len(df.index)+1)
b1 = df.columns[1]
b2 = df.columns[2]
colors = sns.color_palette('husl',n_colors=len(my_range))
# The horizontal plot is made using the hline function
plt.vlines(x=my_range, ymin=df[b1], ymax=df[b2], color='grey', alpha=0.4)
plt.scatter(my_range, df[b2], color=colors, alpha=1 , label=b2, marker='^')
plt.scatter(my_range, df[b1], color=colors, alpha=0.4, label=b1, marker='o')
plt.legend()
# Add title and axis names
plt.xticks(my_range, df['configuration'])
plt.ylabel('epidemic threshold')
plt.xlabel('configuration')
plt.gcf().set_size_inches(8,4)
plt.savefig('epidemic_threshold_comparison.png', bbox_inches='tight')
print('epidemic threshold comparison plotted at epidemic_threshold_comparison.png')
