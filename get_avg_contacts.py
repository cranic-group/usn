import igraph
import numpy as np
import argparse
import os
import sys
from datetime import datetime
from numpy.random import default_rng
from collections import Counter
from itertools import combinations, product
from create_social_base_v2 import preprocess_g2g_matrix, block_pairs_counter_matrix, get_input_data_structures, extract_households, get_population_dictionary
from config.config import GROUP_NAMES, GROUP2GROUP
from lib.libfunc import read_from_config

DEFAULT_IN_PATH = '.'

parser=argparse.ArgumentParser(description="The program creates a Urban Social Network")
parser.add_argument('-c', '--config', dest='config', required=True, type=str, help='configuration file in .ini format')
parser.add_argument('-i', '--input_path', dest='in_path', type=str, default=DEFAULT_IN_PATH, help='input path, used to retrieve all files and directory used by the simulator')
parser.add_argument('--verbose', dest='verbose', action='store_true', default=False, help='whether to print additional info about the population')

args = parser.parse_args()
in_path =  args.in_path
config_file = args.config
verbose = args.verbose

if not os.path.exists(os.path.join(in_path,config_file)):
    print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [ERROR] The configuration file '{os.path.join(in_path,config_file)}' does not exist!")
    sys.exit()   

print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Parameters\n",
      f"\t Configuration file: {config_file}\n",
      f"\t Input path: {in_path}\n",
      )

city = read_from_config(config_file, 'CITY', 'city')
data      = get_input_data_structures(in_path, config_file, city=city)
territory = data['territory']
pop_info  = data['pop_info']
N = pop_info['size']

print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Selected city: {pop_info['city']}")

# Create POP
pop = get_population_dictionary(in_path, config_file, territory, pop_info, verbose=verbose)

seed = read_from_config(config_file, 'RANDOM', 'seed')
# If None, then fresh, unpredictable entropy will be pulled from the OS. (NumPy Documentation)
rng = default_rng(seed)

counter = Counter(pop['type'])

# A[i,j] is the number of different pairs of vertices (u,v) with type(u)==i and type(v)==j -- is a triu matrix
A = block_pairs_counter_matrix([counter[i] for i in range(len(GROUP_NAMES))])
A = np.triu(A)
# A = A + np.triu(A,1).T

# B[i,j] is the polymod-based probability that edge (u,v) exists for any pair (u,v) with type(u)==i and type(v)==j -- is a symmetric matrix
B = preprocess_g2g_matrix(GROUP2GROUP, {'group_names':GROUP_NAMES}, {'type':pop['type']})

# tot_edges is the total expected number of edges
tot_edges = (A*B).sum()

# household subgraph
hh = pop['household']
if hh is None:
    if verbose:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - No households in pop")
    print_HH = False
else:
    hh = extract_households(hh, rng=rng) 
    
    # households:
    edges = [e for hh_ in hh for e in combinations(hh_,2)]
    H = igraph.Graph(N, directed=False)
    H.add_edges(edges)
    if verbose:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Number of households: {len(hh)}") 
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Total number of household edges is {len(edges)}")
    hh_degree = np.mean(H.degree())
    print_HH = True

print('POLYMOD data without home contacts:')
print(f'\t expected number of edges: {tot_edges}')
print(f'\t average degree: {2*tot_edges/N}')
print(f'\t each edge exists with probability: {tot_edges/A.sum()}')
if print_HH: 
    print('HOUSEHOLDS:')
    print(f'\t total number of edges: {H.ecount()}')
    print(f'\t average degree: {hh_degree}')
    print(f'\t each edge exists with probability: {H.ecount()/A.sum()}')
    print('TOTAL:')
    print(f'\t total expected number of edges: {H.ecount()+tot_edges}')
    print(f'\t total average degree: {hh_degree+2*tot_edges/N}')
    print(f'\t each edge exists with probability: {(H.ecount()+tot_edges)/A.sum()}')


