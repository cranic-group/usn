#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>

#define MINARG 5
#define MAXELEMENTS 100000000
#define MAXLINE 1024

int main (int argc, char *argv[]) {
       int done=0, i, j, I, J, b2b_id;
       int max_iter;
       char line[MAXLINE];
       double precision, temp, denom, y_IJ;
       double *k, *x;
       unsigned int n, m, mpairs;
       unsigned int *block;
       FILE *flog = fopen("log.txt", "w");
       srand(time(NULL));
       if(argc<MINARG) {
	  fprintf(stderr,"Usage %s n_nodes n_blocks precision max_iter\n",argv[0]);
	  exit(1);
       }
       n=atoi(argv[1]);
       m=atoi(argv[2]);
       mpairs = (int)(m*(m+1)/2);
       precision=atof(argv[3]);
       max_iter=atoi(argv[4]);
       if(n+mpairs>MAXELEMENTS) {
	fprintf(stderr,"Too many elements: max number is %d\n",MAXELEMENTS);
	exit(1);
       }
       x = (double *)malloc(sizeof(double)*(n+mpairs));
       if(x==NULL) {
	fprintf(stderr,"Could not get memory for %d items of x\n",n+mpairs);
	exit(1);
       }
       k=(double *)malloc(sizeof(double)*(n+mpairs));
       if(k==NULL) {
	fprintf(stderr,"Could not get memory for %d items of k\n",n+mpairs);
	exit(1);
       }
       block=(unsigned int *)malloc(sizeof(unsigned int)*n);
       if(block==NULL) {
	fprintf(stderr,"Could not get memory for %d items of k\n",n);
	exit(1);
       }
       for(i=0; i<n; i++) {
	if(fgets(line,sizeof(line),stdin)==NULL) {
		fprintf(stderr,"Could not get line %d of input\n",i);
		exit(1);
	}
	sscanf(line,"%lf %d %lf",k+i,block+i,&temp);
	x[i] = -log(temp);
       }
       for(j=0; j<mpairs; j++) {
	if(fgets(line,sizeof(line),stdin)==NULL) {
		fprintf(stderr,"Could not get line %d of input\n",i+j);
		exit(1);
	}
	sscanf(line,"%lf %lf",k+i+j,&temp);
	x[i+j] = -log(temp);
       }
       
       // alternative initializations
       // double L = 0.0;
       // for (i=0;i<n;i++){
       //     L += k[i];
       // }
       // L = L/2;
       // printf("L:%lf\n",L);
       // for (i=0;i<n+mpairs;i++){
       //     // x[i] = -log(k[i]/sqrt((float)(n+mpairs)));
       //     x[i] = -log(k[i]/sqrt(2*L)); 
       //     x[i] = rand()/RAND_MAX;  
       // }

       unsigned int iter = 0;
       double *u;
       u = (double *)malloc(sizeof(double)*(n+mpairs));
       double quality;
      
       double num; 
       double *v;
       v = (double *)malloc(sizeof(double)*(n+mpairs));
	   
       // printf("iter %5d: ", iter);
       // for (i=0;i<n;i++){
       //     printf("%10.7f ", x[i]);
       // }
       // printf("   ");
       // for (i=n;i<n+mpairs;i++){
       //     printf("%10.7f ", x[i]);
       // }
       // printf("\n");

       do {
           iter++;
           
           quality = 0.0;
	   memset(u, 0, sizeof(double)*(n+mpairs)); 
	   
	   for (i=0;i<n;i++){
               I = block[i];
               for (j=i+1;j<n;j++){
                   J = block[j];
            	   if(I<J){
            	       b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+J;
            	   } else {
            	       b2b_id = n+J*(m-1)-(int)(J*(J-1)/2)+I;
            	   }
		   // printf("i: %d, j: %d, b2b_id: %d\n", i, j, b2b_id);
                   y_IJ = x[b2b_id];
            	   denom = 1.0+exp(-x[i]-x[j]-y_IJ); // x[i]*x[j]*y_IJ; // 
	           // printf("summed vid %d and blockid %d at index %d\n", j, b2b_id, i);
                   u[i] += exp(-x[j]-y_IJ)/denom; // x[j]*y_IJ/denom; // 
	           // printf("summed vid %d and blockid %d at index %d\n", i, b2b_id, j);
		   u[j] += exp(-x[i]-y_IJ)/denom; // x[i]*y_IJ/denom; // 
	           // printf("summed vid %d and vid %d at block index %d\n\n", i, j, b2b_id);
            	   // if (iter>200){
		   u[b2b_id] += exp(-x[i]-x[j])/denom; // x[i]*x[j]/denom; // 
		   // }
               }
           }
	   
	   // printf("u at iter %5d: ", iter);
	   // for (i=0;i<n;i++){
           //     printf("%10.7f ", u[i]);
	   // }
	   // printf("   ");
	   // for (i=n;i<n+mpairs;i++){
           //     printf("%10.7f ", u[i]);
	   // }
	   // printf("\n");

	   // printf("x[9]:%15.12f, u[9]:%15.12f\n", x[9], u[9]); 
           for (i=0;i<n+mpairs;i++){
	       temp = x[i];
	       // if ((i<n)||(iter>100)){
	       x[i] = -log(k[i]/u[i]); // k[i]/u[i]; // 
               // printf("%lf %lf\n", temp, x[i]);
	       // }
	       quality += (temp-x[i])*(temp-x[i]);
	   }
	   // printf("quality^2=%lf\n", quality);
           quality = sqrt(quality);
	   // printf("quality=%lf\n", quality);
	   
	   
	   // printf("iter %5d: ", iter);
	   // for (i=0;i<n;i++){
           //     printf("%10.7f ", x[i]);
	   // }
	   // printf("   ");
	   // for (i=n;i<n+mpairs;i++){
           //     printf("%10.7f ", x[i]);
	   // }
	   // printf("\n");

       } while (quality>=precision && iter<max_iter);
           
       // compute expected degrees based on the obtained values:
       memset(v, 0, sizeof(double)*(n+mpairs)); 
       for (i=0;i<n;i++){
           I = block[i];
           for (j=i+1;j<n;j++){
               J = block[j];
               if(I<J){
                   b2b_id = n+I*(m-1)-(int)(I*(I-1)/2)+J;
               } else {
                   b2b_id = n+J*(m-1)-(int)(J*(J-1)/2)+I;
               }
               y_IJ = x[b2b_id];
               denom = 1.0+exp(-x[i]-x[j]-y_IJ); // x[i]*x[j]*y_IJ; // 
               num = exp(-x[i]-x[j]-y_IJ); // x[i]*x[j]*y_IJ; // 
               v[i] += num/denom;
               v[j] += num/denom;
               v[b2b_id] += num/denom;
           }
       }
       // compute maximum absolute degree error
       double made=0.0;
       for (i=0;i<n+mpairs;i++){
           if (fabs(v[i]-k[i])>made){
               made = fabs(v[i]-k[i]);
           }
       }
       printf("Iter=%d, precision=%lf, made=%lf\n", iter, quality, made);
       // imposed and expected degree for the first 20 vertices of the network
       int w;
       if (n+mpairs<300){
           w = n+mpairs;
       } else {
	   w = 20;
       }
       printf("%8s %8s\n", "k*", "<k>");
       for (i=0;i<w;i++){
           printf("%d: %8.3f %8.3f\n", i, k[i], v[i]);
       }
       
       fclose(flog);
       return 0; //status;
}
