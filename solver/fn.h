#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_multiroots.h>

struct yf_params {
         double *k;
	 unsigned int *block;
	 unsigned int n, m;
};
     
int yf (const gsl_vector *x, void *params, gsl_vector *f);
int yf_deriv (const gsl_vector *x, void *params, gsl_matrix *df);
int yf_fdf (const gsl_vector *x, void *params, gsl_vector *f, gsl_matrix *df);
int print_state (unsigned int iter, gsl_multiroot_fdfsolver *s, unsigned int n, FILE *f);
