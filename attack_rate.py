#!/usr/bin/env python
# coding: utf-8

# In[45]:


import os, sys
import pickle
import pandas as pd
import numpy as np
from scipy.sparse.linalg import eigsh
import igraph
from collections import Counter
import itertools

# PLOTTING
import seaborn as sns
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, mark_inset

##########
from network_epidemics import *

##########
from edmonds import edmonds


# In[69]:

sns.set_style('whitegrid', rc={'figure.figsize':(17,6)})
sns.set_context("talk")

############## MACROS ###############
graph_fname = 'data/graphs/viterbo/mu20_EP1_t500_min10pop/sb_graph.pickle'
main_out_path = 'output/epidemics/viterbo/new_paper/all_epidemic/random/'
epidemics_paths = {float(dirname.rsplit('_',1)[1]): main_out_path+f'act_prob_{dirname.rsplit("_",1)[1]}/' for dirname in os.listdir(main_out_path) if 'act_prob' in dirname}
city = 'viterbo'
delta = 1/3
nruns = 100
G = igraph.read(graph_fname)
vcount = G.vcount()

## define function to compute the variability measure for a given set of attack rate values
def compute_delta(r):
    return np.sqrt((r**2).mean()-(r.mean())**2)/r.mean()

data = {'p':[], 'beta':[], 'attack rate':[], 'peak time':[], 'epidemic peak':[], 'outcome':[]}

for combo in sorted(epidemics_paths.keys()):
    epidemics_path = epidemics_paths[combo]
    print(f'at {combo}')
    plots_path = epidemics_path+'plots/'
    if not os.path.exists(plots_path):
        os.makedirs(plots_path)

    ## select files storing the result of the epidemic simulation
    filenames = []
    for root, dirs, files in os.walk(epidemics_path):
        for file in files:
            if file.endswith("iterations.pickle") and 'beta' in file:
                filenames.append(os.path.join(root, file))

    ## get iteration data containing information about the epidemic outbreak and extract the attack rate 
    for filename in filenames:
        fname = filename.rsplit('/',1)[-1]
        i = fname.find('beta')
        beta = float(fname[i+4:i+4+fname[i+4:].find('_')])
        with open(filename, 'rb') as f:
            iters = pickle.load(f)
        i_t = [it['node_count'][2] for it in iters]
        time2peak = np.argmax(i_t)
        peak = i_t[time2peak]
        r = iters[-1]['node_count'][3]/vcount
        data['p'].append(combo)
        data['beta'].append(beta)
        data['attack rate'].append(r)
        data['peak time'].append(time2peak)
        data['epidemic peak'].append(peak)
        if r>0.25:
            data['outcome'].append('outbreak')
        else:
            data['outcome'].append('extinction')

    ## build and preview the dataframe
    df = pd.DataFrame(data)
    # print(df.describe())
    # print(df.head())

plots_path = plots_path.rsplit('/',3)[0]+'/plots/'
if not os.path.exists(plots_path):
    os.makedirs(plots_path)

data_df = pd.DataFrame(data)

#### outbreak probability ####
outcomes = data_df.groupby(['p','outcome']).size().to_dict()
ps = sorted(set(data_df['p']))
outbreak_probs = [outcomes[(p,'outbreak')]/(outcomes[(p,'outbreak')]+outcomes[(p,'extinction')]) for p in ps]
plt.clf()
sns.lineplot(x=ps, y=outbreak_probs) 
plt.xticks(ps) 
plt.gcf().set_size_inches(8,4)
plt.savefig(plots_path+'outbreak_probability_comparison.png', bbox_inches='tight')
print(f'outbreak probability comparison plotted at {plots_path+"outbreak_probability_comparison.png"}')

#### attack rate ####
plt.clf()
g = sns.lineplot(data=data_df.query("outcome=='outbreak'"), x='p', y='attack rate') #, hue='outcome')
# g.set(yscale='log')
plt.xticks(sorted(set(data_df['p']))) # my_range, df['configuration'])
plt.gcf().set_size_inches(8,4)
plt.savefig(plots_path+'attack_rate_comparison.png', bbox_inches='tight')
print(f'attack rate comparison plotted at {plots_path+"attack_rate_comparison.png"}')

#### epidemic peak ####
plt.clf()
g = sns.lineplot(data=data_df.query("outcome=='outbreak'"), x='p', y='epidemic peak') #, hue='outcome')
# g.set(yscale='log')
plt.xticks(sorted(set(data_df['p']))) # my_range, df['configuration'])
plt.gcf().set_size_inches(8,4)
plt.savefig(plots_path+'epidemic_peak_comparison.png', bbox_inches='tight')
print(f'epidemic peak comparison plotted at {plots_path+"epidemic_peak_comparison.png"}')

#### peak time ####
plt.clf()
g = sns.lineplot(data=data_df.query("outcome=='outbreak'"), x='p', y='peak time') #, hue='outcome')
# g.set(yscale='log')
plt.xticks(sorted(set(data_df['p']))) # my_range, df['configuration'])
plt.gcf().set_size_inches(8,4)
plt.savefig(plots_path+'peak_time_comparison.png', bbox_inches='tight')
print(f'peak time comparison plotted at {plots_path+"peak_time_comparison.png"}')
