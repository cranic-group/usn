import os
from itertools import combinations
from pathlib import Path
import argparse
import pickle
import igraph as ig
import numpy as np
from numpy.random import default_rng
from datetime import datetime

from lib.libfunc import read_from_config
from create_social_base import compute_households, extract_households, get_input_data_structures


def dump_pop(g, filename='prova.csv'):
    order = np.argsort(g.vs['tile_id'])
    tiles = np.asarray(g.vs['tile_id'])[order]
    types = np.asarray(g.vs['type'])[order]
    with open(filename,'w') as f:
        print('tile_id,x,y,type', file=f)
        for tile_id,type_id in zip(tiles,types):
            print(f'{tile_id},0,0,{type_id}', file=f)
    return order


OUTPUT_DIR = "output"
DEFAULT_CONFIG_FILE = 'config/config.ini'
DEFAULT_IN_PATH = '.'
BF_FILE='BuildSBG/bf'

def main():
    parser=argparse.ArgumentParser(description="The program adds the household layer to an already existing Urban Social Network")
    parser.add_argument('-c', '--config', dest='config', default=DEFAULT_CONFIG_FILE, type=str, help='configuration file in .ini format')
    parser.add_argument('-o', '--output', dest='output_dir', default=OUTPUT_DIR, type=str, help='output directory where dumps are stored')
    parser.add_argument('--city', dest='city', type=str, default='', help='specify the city')
    parser.add_argument('-i', '--input_path', dest='in_path', type=str, default=DEFAULT_IN_PATH, help='input path, used to retrieve all files and directory used by the simulator')
    parser.add_argument('-g', '--graph', dest='graph', type=str, help='filename of the input graph, must be compatible with igraph.read()')

    args=parser.parse_args()
    in_path = args.in_path
    config_file = args.config
    city = args.city
    output_dir = args.output_dir
    graph_file = args.graph
    
    print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Running on {graph_file}")

    # intra-tile distance and distance function
    seed = read_from_config(config_file, 'RANDOM', 'seed')
    if seed is None:
        seed=default_rng(seed).integers(10000)
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - None seed in config file, extracted seed is {seed}")
    data      = get_input_data_structures(in_path, config_file, city=city)
    dist_info = data['dist_info']
    zero_dist = dist_info['zero_dist']
    dist_func = dist_info['dist_func']
    
    g = ig.read(graph_file)
    to_remove = []
    for e in g.es:
        if e['layer'] == 'household':
            to_remove.append(e.index)
        elif 'household' in e['layer']:
            e['layer'] = e['layer'].replace('household','')
    print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Deleting {len(to_remove)} edges from the graph")
    g.delete_edges(to_remove)
   
    while True:
        temp_pop_file = f'pop_file_{np.random.randint(100000)}.txt'
        if not os.path.exists(temp_pop_file):
            break
    order = dump_pop(g, temp_pop_file)
    hh = compute_households(in_path, bash_command=BF_FILE, pop_filename=os.path.abspath(temp_pop_file), seed=seed, verbose=True)
    os.remove(temp_pop_file)
    hh = hh[order.argsort()]
    g.vs['household'] = hh
    hh = extract_households(hh)
    edges = [e for hh_ in hh for e in combinations(hh_,2)]
    layer = 'household'
    if 'euclidean' in dist_func:
        distance = zero_dist
    elif dist_func == 'one':
        distance = 1
    else:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [ERROR] Unknown distance function! Abort", sys.stderr)
        sys.exit(111)
    print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Number of households: {len(hh)}")
    print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Adding {len(edges)} household edges to the graph")
    if sorted(g.edge_attributes())!=['distance','layer']:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [WARNING] The graph has the attributes {g.edge_attributes()} whereas newly added edges will have the attributes 'distance' and 'layer'")
    g.add_edges(edges, {'layer':layer, 'distance':distance})
    g.simplify(combine_edges={'layer':'concat', 'distance':'first'})

    # out_file = os.path.join(output_dir,Path(graph_file).stem)+'_new.pickle'
    out_file = graph_file.rsplit('.',1)[0]+'_new.pickle'
    with open(out_file, "wb") as fout:
        pickle.dump(g, fout, protocol=pickle.HIGHEST_PROTOCOL)    

if __name__ == "__main__":
    main() 

