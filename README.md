### WARNING: The code is under continuous development and some features may not yet be documented or conversely may have become obsolete. Please contact us if you encounter any problems!

# <a name="top"></a> Urban Social Network Generator
This repository contains the code to generate a data-driven model for urban social networks as described in  [1](#inferring). By using just widely available aggregated demographic and social-mixing data, we are able to create, for a territory of interest, an age-stratified and geo-referenced synthetic population whose individuals are connected by “strong ties” of two types: intra-household (e.g., kinship) or friendship. While household links are entirely data-driven, we propose a parametric probabilistic model for friendship, based on the assumption that distances and age differences play a role, and that not all individuals are equally sociable. 


The repository also contains the code (`network_epidemics.py`)  to simulate the spread of a disease on the `usn`. 
We used the desease diffusion process to evaluate the effecctiveness of our model and described some preliminary results in two papers. In [2](#epidemics) we characterize our network model under selected configurations and we show its potential as a building block for the simulation of infections' propagation. In [3](#venues) We focused on the greatest park of the City of Florence, Italy, to provide experimental evidence that our simulator produces contact graphs with unique, realistic features, and that gaining control of the mechanisms that govern interactions at the local scale allows to unveil and possibly control non-trivial aspects of the epidemic.


**Important Note: while the code is fully configurable to work with different input this repository only contains the data to reproduce the results published in the referenced papers.**


[1] <a name='inferring'> [Inferring Urban Social Networks from Publicly Available Data](https://www.mdpi.com/1087824) </a>\
[2] <a name='epidemics'>[A Model for Urban Social Networks](https://link.springer.com/chapter/10.1007/978-3-030-77967-2_23)</a>\
[3] <a name='venues'> [DATA-DRIVEN SIMULATION OF CONTAGIONS IN PUBLIC VENUES](https://www.researchgate.net/publication/352121181_DATA-DRIVEN_SIMULATION_OF_CONTAGIONS_IN_PUBLIC_VENUES)</a><br/>


## Index

0. [Overview](#overview)
1. [Prerequisites](#prerequisites) 
2. [Generate Population](#population)
3. [Households creation](#households)
3. [Urban Social Network - USN](#usn)
4. [Epidemic Spreading](#epidemic)
5. [Data, Analysis and Plots](#analysis)


## <a name="overview"></a> 0. Overview ([top](#top))
The code is all python3 with the exception of the program that recreate the households which is written in C.  Assuming you have correctly installed the software (see section [Prerequisites](#prerequisites)) the first step is to generate the population file of the selected territory, as described in section [Generate Population](#poopulation). Here we added the data file relative to the city of Florence, Viterbo and Sabaudia. Once you have the population, before actually generating the USN you must compile the housholds subprogram following the instruction in [Households creation](#households). Finally, to generate the USN graph you can use `usn.py`, section [Urban Social Network](#usn). To simulate a disease diffusion process on the USN refer to the last two sections [4](#epidemic) and [5](#analysis).


## <a name="prerequisites"></a> 1. Prerequisites ([top](#top))
We tested the software on Linux (ubuntu, mint) and Osx. Overall the installation procedure should be easy as the whole project is developed with python3. However, it is worth noting that the setup needs some special attention because some python libraries, expecially those for the manipulation of geospatial data, may requires the installation of system libraries. For example, on some system, you have to install GDAL and PROJ (refer to https://gdal.org and https://proj.org for specific installation instructions for your system). Moreover, some python packages like scipy may require the installation of other libraries depending on your system (for example libjpeg and zlib on fresh new linux and osx) while igraph requires cmake and bison. 

### Python Packages
The file named `config/requirements.txt` contains the list of Python packages required by the program. To get them proceed as follow:

```sh
pip3 install -r config/requirements.txt
```


## <a name="population"></a> 2. Generate Population ([top](#top))
To create the USN network you need to feed the generator with a population file which stores the geographical coordinates of each person in the chosen territory. To generate the population file of a given city/territory of interest use the bash/python scripts inside the **scripts** directory. Perform the following steps to generate the file:

#### Download TIF files
Run the script *download\_wordpop\_tiff.sh* to download TIF files from [WorldPop](https://www.worldpop.org). The script is configured to download the Italian population (about 5.3GB and 36 files). You can use the script structure to download the files of another country. You can proceed as follow to download the files and store them in the **worldpopData** directory.

    ```sh
    ./download_wordpop_tiff.sh
    mkdir worldpopData
    mv *.tif worldpopData
    ```

#### Territory and Population
Run the script **gen\_population.sh** to generate the files needed to create the population. The script takes 2 parameters: 1) the name of the city; 2) the directory containing the TIF files obtained from WorldPop.

```sh
./gen_population.sh "Trento" worldpopData
```
The files generated by the script are stored in `data/population/CITY_NAME`, population is divided in age classes based on WorldPop data. 

#### Output file generated by gen\_population.sh
To check that everything went well the script writes some output files that permit to visualize the resulting territory/population. The default administration level value is set to 8.

	1. city.geojson

A geopandas dataframe containing the borders of the generated city. 

	2. city.geojson.pdf
    
A pdf version of the latter, added to check the borders of the territory.

	3. geodata_centers.pdf

Plot of the reconstructed grid of tiles that covers the territory

	4. population_density.pdf

A pdf portraying the density of the territory according to WorldPop.

	5. pop_geodata_plot.pdf

A pdf with the positions of the actors

	6. simplified_shape_plot.pdf

The territory itself.

	7. territory_projected.pickle
	8. territory_not_projected.pickle

Two pickles containing the boundary box, the borders, the dimensions, the center coordinates, the number of the tiles and the tile size of the territory both projected or in WSG coordinates. The tiles dimension by which the territory is subdivided can be set in ```config/config.py``` (see [Advanced Usage](#advancedusage)).

	9. tiff_info.pickle

A report file with information about the tiff file(s) employed to get the population from the territory.

	10. population_dataframe.pickle
The dataframe with the data of the population. For each point of the territory a distribution of persons by age and sex is given.

	11. population_{city name}.csv
The csv containg the data of the population. Each entry represents an actual actor. The columns are:
1. **tile_id**: the id of the tile the actor belongs to;
2. **x**: the x position of the actor;
3. **y**: the y position of the actor;
4. **type**: an index corresponding to the age group the actor belongs to.


#### Add different age stratification    
You can also use ISTAT data to divide your population in age classes. The repository contains such data for few cities, they are stored in `data/population/istat2020/`. To use these data you can use the python script **genIstatPop.sh**. 
    
```sh
genIstatPop.sh trento
```

## <a name="households"></a> 3. Recreate Households ([top](#top))
A necessary step before generating the USN graph is the creation of the Households. The directory named `BuildSBG` contains the necessary program. You don't need to run it but just to compile it because it's directly invoked by the program that generates the USN (`usn.py`). To compile procede as follows:

```sh
cd BuildSBG
make clean
make all
```

#### Household distribution
To recreate the households of a given territory the distribution of roles and households must be specified. This data are available for many countries, the current files refer to Italy and are taken from ISTAT.

The code uses three different files:
```
BuildSBG/hhroleprob.csv
BuildSBG/hhfamilyprob.csv
BuildSBG/hhvarieprob.csv
```
First file is ```BuildSBG/hhroleprob.csv```. It holds the probability of each actor in function of its age group.
We picked 7 different roles: FCf (son of a couple with children), FM (son of a single parent), C (couple), GCf (parent in couple with children), GM (single parent), V (various), PS (single person).
For each role a different household can be made. The household distribution can be set in ```BuildSBG/hhfamilyprob.csv``` and ```BuildSBG/hhvarieprob.csv```.
For each role a family is made via bf.c. The rules for the household formation are hard coded both in ```BuildSBG/ bf.c``` and ```BuildSBG/ bf_mod.c```.



## <a name="usn"></a> 4. Urban Social Network ([top](#top))
This is the main script which generates the Urban Social Network starting from the newly created population file.
To run the simulation:
```sh
python usn.py -c config/config.ini -o output/dir
```
Where ```config/config.ini``` is the configuration file, and ```output/dir``` is the output directory where the graph is stored.
Other options of the software are:
```
-i in/path
```
Is the main directory, relative to which all the data regarding the territory is stored. The default value is the directoy containing ```usn.py```.
```
--filter_pop [FILTER_POP]
```
This option filter tiles based on [FILTER_POP] which represents the minimum number of resident in a certain tile.
```
--verbose
```
Prints data about the families. This command is mandatory if one wants to make a complete report of the generated graph.
Further documentation is available via the command:
```
python usn.py --help
```

#### Standard Configuration

By modifing the configuration file is possible to set the main feature of the Urban Social Network, without changing the age groups and the contact matrix between age groups, which is taken from [SOCRATES](https://bmcresnotes.biomedcentral.com/articles/10.1186/s13104-020-05136-9).
This configuration file can be found here:
```sh
config/config.ini
```
Where all the instruction are provided.

#### <a name="advancedusage"></a> Advanced Configuration
The age groups and the probability of contact within groups can also be changed trought a different configuration file. As this is python file this is not recomanded for non experienced users. The python configuration file can be found here:
```sh
config/config.py
```
The population paragraph contains information about the division of the population into groups. The list `GROUP_NAMES` contains the names of each age group. `AGE_GROUPS` is a list with the age intervals for each age group. In the standard case the age groups are subdivided in this way:
```
children	0  - 18
young		19 - 35
adults		36 - 65
elderly		66+
```

`GROUP2GROUP` is the contact matrix taken from [SOCRATES](https://bmcresnotes.biomedcentral.com/articles/10.1186/s13104-020-05136-9). Each entry is average number of contacts per day between different age groups. Keep in mind that the lenght of `AGE_GROUPS`, the lenght of `GROUP_NAMES`, and the columns and the rows of `GROUP2GROUP` must be equal.
`TX` and `TY` are the parameters which set the dimensions of the grid the territory is divided into (in meters). These parameters will be crucial when the population is generated. The default value is set to 500m for both `TX` and `TY`.


#### Report
A *Jupyter notebook* is available in ```report/``` where all the instructions are provided. The purpose of this notebook is to give to the user insights on the input and the relative output.



## <a name="epidemic"></a> 5. Epidemic Spreading ([top](#top))

You can run a SIR model on the network built with `usn` using the program `network_epidemics.py`. The program uses the  [NDlib - Network Diffusion Library](https://ndlib.readthedocs.io/en/latest/). To run the progam a configuration file and few command line parameters are needed. The initial infected individuals of the population are choosen at random, you can change this behaviour using the parameter `--mode`.

#### Configuration File
The configuration file contains a single section named `[EPIDEMICS]`. The section contains the following parameters: `pi`, `delta`, `beta`, `gamma` and `colors`. 

 * `pi` defines the number of initial infected, it must be a number between 0 and the number of nodes in the network. Instead of defining `pi` it is possible to define `delta`. `delta` defines the fraction of intial infected nodes, it must be a number between 0 and 1.
 * `beta` defines the edges infection probability, it must be a number between 0 and 1.
 * `gamma` defines the recovery probabilty, it must be a number between 0 and 1.
 * `colors` defines the colors used for each epidemic status of the SIR plot.

 The following is an example of configuration file:

 ```
 [EPIDEMICS]
 pi    = 100
 delta = .001
 beta  = .03
 gamma = .1
 ```

#### Command Line Parameters
**-c CONFIG, --config CONFIG**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;configuration file in .ini format.  

**-o OUT\_PATH, --output-path OUT\_PATH**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;output path, used for all output files generated as a result of the simulation. 
 
**-i IN\_PATH, --input-path IN\_PATH**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;input path, used to retrieve all files and directory used by the simulator.  

**-g GRAPH, --graph GRAPH**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the graph to be used for the epidemic simulation. Use the graph generated by `usn` that has been saved as `pickle` file.  

**-n N\_ITER, --n-iter N\_ITER**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;number of steps of the epidemic simulation; if falsy, the simulation runs until the number of infected individuals becomes 0.  

**-m MODE, --mode MODE**   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the execution mode: can be "random", "node_id" or the name of a vertex attribute; in the latter case, all initially infected vertices have the same (randomly chosen) value for that attribute.  

**-u VALUE, --mode-value VALUE**   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the specific value of the mode attribute, only the modes tile_id and node_id are supported  

**-r N\_RUNS, --n-runs N\_RUNS**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;number of runs to execute. Each run will last for the number of iterations defined with `--n_iter`  

**-b BETA, --beta BETA**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;edges infection probability. This parameter overwrite the `beta` value specified in the configuration file.   

**-v, --verbose**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;whether to print information about the simulation.  

#### Output
The output of the program are two charts (`hmtl` files) and a `pickle` file containing the information of the simulation.

## <a name="analysis"></a> 5. Data, Analysis and Plots ([top](#top))

You can use `analysis.py` to save in a single file the data obtained from `usn` and  `network_epidemics.py`. The file will contain a dictionary in the following format: 

```
{ 
  'tile_id': [A list containing the tile id of each person], 
  'hh_id': [A list containing the household id of each person],  
  'type_id': [A list containing the type id of each person], 
  'sim_steps': [A list of list containing the epidemic status 
  		of each person at each step of the simulation. The status
  		is a number that can be used as index to retrive the name
  		of the status from the list stored in 'epidemic_statuses'.
  		Notice that the number of the status is shifted by 1 position, 
  		thus to get the name of status i, you must access the positio i-1.],
  'groups_name': [A list containing the names of each age group],
  'groups_size': [A dictionary containing the number and size 
  		of each age group. 
  		Numbers are the index of the list stored in 'groups_name' 
  		and can be used to retrieve the name of the group],   
  'epidemic_statuses': [A list containing the names 
  		of each epidemic status],
  'pop_size': The number of nodes/persons, 
  'city': The name of the city, 
  'tiles': [A list containing the x e y length of each tile], 
  'nHouseholds': The number of households, 
  'nIsolated': The number of person in any household
  t }
```
 
The program `analysis.py` uses a configuration file and few command line parameters. You can use it to create the data structure discussed above with the option `--dump` or to plot the data using the option `--plot`. 
 
#### Configuration File
The configuration file contains a single section named `[MAIN]`. The section contains the following parameters: `epidemic_statuses`, `groups_name` and `ntiles`. 
 
#### Command Line Parameters
**-g G\_FILE, --graph-file G\_FILE**   
&nbsp;&nbsp;&nbsp;&nbsp;The file containing the graph representing the urban social network. Use the `pickle` file generate by `usn` 
 
**-e E\_FILE, --epidemic-file E\_FILE**   
&nbsp;&nbsp;&nbsp;&nbsp;The file generated by the program `network_epidemics.py`, containing the information of the epidemic simulation.   

**-c CONFIG\_FILE, --config-file CONFIG\_FILE**   
&nbsp;&nbsp;&nbsp;&nbsp;The configuration file read by the program and discussed above.  

**-o OUTPUT\_DIR, --output OUTPUT\_DIR**   
&nbsp;&nbsp;&nbsp;&nbsp;The directory where all output file will be stored.   

**-y CITY, --city CITY**    
&nbsp;&nbsp;&nbsp;&nbsp;The name of the city.  

**--plot**  
&nbsp;&nbsp;&nbsp;&nbsp;If you want to plot the data. 
  
**--dump**     
&nbsp;&nbsp;&nbsp;&nbsp;If you want generate the data.  
