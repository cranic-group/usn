import igraph
import numpy as np
import argparse
from collections import Counter
from create_social_base import preprocess_g2g_matrix, pairs_counter_matrix
from config.config import GROUP_NAMES, GROUP2GROUP

GRAPH_FILE = 'data/graphs/sabaudia_graph_mu5_ep05_t1000.pickle'

parser=argparse.ArgumentParser(description="The program recaps a few parameters that are useful to generate a randomized version of the input graph")
parser.add_argument('-g', '--graph', dest='graph', type=str, default=GRAPH_FILE, help='input graph')
args = parser.parse_args()
graph_file = args.graph
print(f'reading the following graph file: {graph_file}')

G = igraph.read(graph_file) 
counter = Counter(G.vs['type'])

# A[i,j] is the number of different pairs of vertices (u,v) with type(u)==i and type(v)==j -- is a triu matrix
A = pairs_counter_matrix([counter[i] for i in range(len(GROUP_NAMES))])
# A = A + np.triu(A,1).T

# B[i,j] is the polymod-based probability that edge (u,v) exists for any pair (u,v) with type(u)==i and type(v)==j -- is a symmetric matrix
B = preprocess_g2g_matrix(GROUP2GROUP, {'group_names':GROUP_NAMES}, {'type':G.vs['type']})

# tot_edges is the total expected number of edges
tot_edges = (A*B).sum()

# household subgraph
H = G.subgraph_edges([e.tuple for e in G.es if e['layer']=='household'], delete_vertices=False)
hh_degree = np.mean(H.degree())

print('POLYMOD data without home contacts:')
print(f'\t expected number of edges: {tot_edges}')
print(f'\t average degree: {2*tot_edges/G.vcount()}')
print(f'\t each edge exists with probability: {tot_edges/A.sum()}')
print('HOUSEHOLDS:')
print(f'\t total number of edges: {H.ecount()}')
print(f'\t average degree: {hh_degree}')
print(f'\t each edge exists with probability: {H.ecount()/A.sum()}')
print('TOTAL:')
print(f'\t total expected number of edges: {H.ecount()+tot_edges}')
print(f'\t total average degree: {hh_degree+2*tot_edges/G.vcount()}')
print(f'\t each edge exists with probability: {(H.ecount()+tot_edges)/A.sum()}')


