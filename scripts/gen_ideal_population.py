import sys
import numpy as np
import pandas as pd
from numpy.random import default_rng
import pyproj
from pyproj import CRS
from pyproj import Transformer
import geopandas as gpd
from shapely.geometry import Polygon, Point


from build_geojson_from_bbox import *
from compute_territory import build_projected_territory, project_coord, get_info_for_territory
from config import INPUT_CRS, OUTPUT_CRS, TERRITORY_PICKLE_FILE_PROJECTED, TERRITORY_PICKLE_FILE_NOT_PROJECTED, GROUP_FREQ
from plot_geojson import plot_geojson

import math

try:
    from city_config import TILE, NTILES, GENTYPE, SEED, CITY_NAME, SAMPLE_SIZE
    print('options read from city_config.py')
except:
    TILE   = (200,200)
    # Ntiles is used only to define the raidus! Below is recomputed as radius / tile
    NTILES = (2,2)
    GENTYPE = "uniform" # can be radial or uniform 
    SEED    = 1
    SAMPLE_SIZE = 100
    CITY_NAME = f'idealcity_{GENTYPE}_{SAMPLE_SIZE}'

ORIGIN = (41.89, 12.51)
RADIUS = NTILES[0]*TILE[0]/2
NPC    = 25 # Half points in the polygon used to approximate the circle of the city
BBOX_GJ = 'idealcity.geoj'
BBOX_GJ_FIG =  BBOX_GJ + '.pdf'
GEOJ_FILE = CITY_NAME + '.geojson'
POP_PLOT  = CITY_NAME + '_population_plot.pdf'
MATH_PI = math.pi
CONFIG_FILE = "../config/config.ini"
POP_CSV_FILE  = 'population'


def listofpoints_to_geoseries(points, crs):

    geo_points = [Point(p) for p in points]
    geos = gpd.GeoSeries(geo_points, crs=crs)  # you should also specify CRS

    return geos

def gen_pop(territory_projected,  Rcity, pop_size=1000, gentype='uniform', rng=None):
    """
        # COORDS AS LON, LAT (geopandas style): x = LON, y = LAT
        bbox = ( min_lon, min_lat, max_lon, max_lat) ---> (min_x, min_y, max_x, max_y)
    """

    rng = default_rng(rng)

    # brders = (max_x, min_x, max_y, min_y)
    max_x, min_x, max_y, min_y = territory_projected['borders']
    x0, y0 = min_x, min_y 
    tx = territory_projected['tile'][0]
    ty = territory_projected['tile'][1]
    ntx = territory_projected['ntiles'][0]
    nty = territory_projected['ntiles'][1]
    cx, cy = territory_projected['center']


    if gentype == 'uniform':
        #xarray = rng.uniform(min_x, max_x, pop_size)
        #yarray = rng.uniform(min_y, max_y, pop_size)
        radius = np.sqrt(rng.uniform(0, Rcity*Rcity, pop_size))
        teta = rng.uniform(0, 2*MATH_PI, pop_size)
        x = []
        y = []
        for r, t in zip(radius, teta):
                x.append(r*math.cos(t)+cx)
                y.append(r*math.sin(t)+cy)
        xarray = np.array(x)
        yarray = np.array(y)

    elif gentype == 'radial' or gentype == 'radial_decreasing':
        radius = rng.uniform(0, Rcity, pop_size)
        teta = rng.uniform(0, 2*MATH_PI, pop_size)
        x = []
        y = []
        for r, t in zip(radius, teta):
                x.append(r*math.cos(t)+cx)
                y.append(r*math.sin(t)+cy)
        xarray = np.array(x)
        yarray = np.array(y)
    
    elif gentype == 'radial_increasing':
        #xarray = rng.uniform(min_x, max_x, pop_size)
        #yarray = rng.uniform(min_y, max_y, pop_size)
        radius = np.power(rng.uniform(0, Rcity*Rcity*Rcity, pop_size), 1/3)
        teta = rng.uniform(0, 2*MATH_PI, pop_size)
        x = []
        y = []
        for r, t in zip(radius, teta):
                x.append(r*math.cos(t)+cx)
                y.append(r*math.sin(t)+cy)
        xarray = np.array(x)
        yarray = np.array(y)

    else:
        print(f"Error wrong generetor type: {gentype}")
        sys.exit(111)


    xtile  = (xarray - x0)//tx
    ytile  = (yarray - y0)//ty
    tileid = xtile + ntx*ytile
    tileid = tileid.astype(int)
    #for i in tileid:
    #    print(f'i={i}, xtile, ytile ={xtile[i]}, {ytile[i]}, TILEID = {tileid[i]}')
    txcoord = xtile*tx + tx/2.0 + min_x
    tycoord = ytile*ty + ty/2.0 + min_y

    return xarray, yarray, tileid, txcoord, tycoord



# From wgs84 to wgs84_32n
# coord = (lon, lat) (((this function differs from the one in lib with the same name!!)))
def project_coord(coord, input_CRS, outCRS):
    inproj  = pyproj.CRS(input_CRS)
    outproj = pyproj.CRS(outCRS)

    transf = Transformer.from_crs(inproj, outproj)
    a = transf.transform(coord[0], coord[1])
    return a


def create_circle_points(n, center, radius):
    """
        return a list of (x,y) coordinates on the circle centered in center
        with the given radius
    """

    ox, oy = center
    r2 = radius*radius
    ax = np.linspace(-radius, radius, n)
    ay1 = np.sqrt(r2 - ax*ax)
    ay2 = -np.sqrt(r2 - ax*ax)

    circle_points = []

    for x,y in zip(ax, ay1):
        circle_points.append((x+ox, y+oy))
    for x,y in zip(ax[::-1], ay2[::-1]):
        circle_points.append((x+ox, y+oy))
    
    return  circle_points


# In GeoPandas, the coordinates are always stored as (x, y), 
# and thus as (lon, lat) order, regardless of the CRS 
# (i.e. “traditional” order used in GIS).
def geopandas_poligon_from_coord_list(xlist, ylist, crs, write_to_file=False, output_file='tmp.geojson'):

    polygon_geom = Polygon(zip(xlist, ylist))
    polygon = gpd.GeoDataFrame(index=[0], crs=crs, geometry=[polygon_geom])       
    print(polygon.geometry)

    if write_to_file:
        polygon.to_file(filename=output_file, driver='GeoJSON')
        #polygon.to_file(filename='polygon.shp', driver="ESRI Shapefile")

    if not write_to_file:
        return polygon
    else:
        return polygon, output_file


def main():

    center = ORIGIN
    tile   = TILE 
    city   = CITY_NAME
    output_CRS = OUTPUT_CRS
    input_CRS  = INPUT_CRS
    #input_file = sys.argv[1]
    #tile_dim_x = float(sys.argv[2])
    #tile_dim_y = float(sys.argv[3])
    sample_size = SAMPLE_SIZE
    geoj_file   = GEOJ_FILE
    radius      = RADIUS
    # print(f'radius = {radius}m')
    config_file = CONFIG_FILE
    seed        = SEED
    gentype     = GENTYPE

    rng = default_rng(seed)

    # Origin is in (lat, lon) 
    print(f'wgs84_center = {center}, {input_CRS}')
    center_not_pj = (center[0],center[1])
    # Project center coordinates 
    center_pj = project_coord(center_not_pj, input_CRS, output_CRS)
    print(f'projected center: {center_pj}, {output_CRS}')


    # Create a projected circle as geopandas Polygon
    cp = create_circle_points(NPC, center_pj, radius)
    print(f'Number of points in circle: {len(cp)}')

    cp_points = [Point(x, y) for x, y in cp]
    cp_geos   = gpd.GeoSeries(cp_points, crs=output_CRS)
    poly      = Polygon([[p.x, p.y] for p in cp_geos])
    gpoly     = gpd.GeoDataFrame(index=[0], crs=output_CRS, geometry=[poly])       
    circular_geoj_df_pj  = gpoly.copy()

    origin = center_pj[0] - radius, center_pj[1] - radius
    ntiles = (int(2*radius//tile[0]), int(2*radius//tile[1]))
    territory_pj = build_projected_territory(origin, ntiles, tile, input_CRS, output_CRS, origin_is_projected=True)

    print(territory_pj)

    # Gnerate N points with uniform distribution inside a given range in WGS84
    pop_df = pd.DataFrame()
    xarray, yarray, tileid, txcoord, tycoord = gen_pop(territory_pj,  pop_size=sample_size, gentype=gentype, Rcity=radius, rng = rng)
    
    pop_df['x'] = xarray
    pop_df['y'] = yarray
    pop_df['tile_id'] = tileid
    pop_df['txcoord'] = txcoord
    pop_df['tycoord'] = tycoord
    pop_df['coord'] = [(x,y) for x,y in zip(xarray, yarray)]
    pop_df['coord'] = listofpoints_to_geoseries(pop_df['coord'], crs = output_CRS)
    pop_df['centers'] = [(x,y) for x,y in zip(txcoord, tycoord)]
    pop_df['geometry'] = listofpoints_to_geoseries(pop_df['centers'], crs = output_CRS)
    
    # Add type to each individual
    groups = GROUP_FREQ
    group_freqs = list(groups.values())
    pgsum = np.cumsum(group_freqs)
    f = rng.uniform(0, 1, sample_size)
    node_types = np.searchsorted(pgsum, f)
    pop_df['type'] = node_types
    
    pop_df = pop_df.sort_values(by=['tile_id'], ascending=True)

    # # Select points inside the circular geojson
    # if circular_geoj_df_pj is not None:
    #     # Multipolygon to polygon to use the method within
    #     circular_geoj_df_geometry = circular_geoj_df_pj.unary_union
    #     # circular_geoj_df['geometry'] = circular_geoj_df_geometry

    #     # Find the point inside the shape
    #     points_inside = [False]*len(pop_df['coord'])
    #     for i,c in enumerate(pop_df['coord']):
    #         if c.within(circular_geoj_df_geometry):
    #             points_inside[i] = True

    #     pop_df['inside'] = points_inside
    #     pop_df = pop_df.loc[pop_df['inside'] == True] 


    # Dump to csv
    dump_df = pop_df[['tile_id', 'x', 'y', 'type']]
    pop_csv_file = POP_CSV_FILE + '_' + city.lower().replace(" ","_").replace("'","") + '.csv'
    dump_df = dump_df.round(1)
    dump_df.to_csv(pop_csv_file, index=False)
    print("population data written to {} ".format(pop_csv_file))

    if sample_size > len(pop_df):
        sample_size = len(pop_df)

    if 1:
        # First, the bounding box
        maxx, minx, maxy, miny = territory_pj['borders']
        bbox = box(minx, miny, maxx, maxy)
        #print(bbox)
        b = gpd.GeoSeries(bbox, crs=output_CRS)
        # convert to CTX MAP CRS
        b = b.to_crs(epsg=3857)

        # The circual geometry is already a geojson just convert it
        g = circular_geoj_df_pj.to_crs(epsg=3857)
        
        # tile centers
        tc = gpd.GeoSeries([Point(x) for x in pop_df['centers']], crs=output_CRS)
        tc = tc.to_crs(epsg=3857)

        # add population
        if sample_size == 0 or sample_size > 1000:
            sample_size = 1000
        pop_df_sample = pop_df.sample(n=sample_size)
        points = [Point(x, y) for x, y in zip(pop_df['x'], pop_df['y'])]
        p = gpd.GeoSeries(points, crs=output_CRS)
        p = p.to_crs(epsg=3857)
        
        ax = b.plot(alpha=0.3, edgecolor='g', linewidth=4)
        g.plot(ax=ax, alpha=0.2, edgecolor='r', linewidth=4)
        p.plot(ax=ax, alpha=0.5, edgecolor='black', marker='o', markersize=1)
        #tc.plot(ax = ax, alpha=0.5, edgecolor='black', marker='o', markersize=10)


        # grid = build_grid_dictionary (territory)
        # Plot GRID
        # polylist=[]
        # for k,v in grid.items():
        #    poly = v['vertices'] + [v['vertices'][0]]
        #    polylist.append(Polygon(poly))
            #xtile, ytile = zip(*poly)
            #plt.plot(xtile, ytile, 'r', linewidth=0.1, ax=ax)
            # ctx, cty = v['coord']
            #plt.plot(ctx, cty, 'b+', markersize=1, ax=ax)
        #poly = gpd.GeoSeries(polylist, crs=input_CRS)
        #poly.plot(alpha=0.2, edgecolor='r', ax=ax)

        ctx.add_basemap(ax, zoom = 13) #source=ctx.providers.OpenStreetMap.Mapnik, crs=b.crs.to_string()) #crs=geo_df.crs.to_string()
        plt.savefig(POP_PLOT)
        print("Bbox, shape and tiff data centers saved to {}".format(POP_PLOT))


if __name__ == '__main__':
    main()
