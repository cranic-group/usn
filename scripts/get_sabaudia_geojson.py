import sys
import requests
import geojson
import osm2geojson

def get_geojson(city, debug=False, dump=True):


    url = 'https://overpass-api.de/api/interpreter?data='
    #query = '[out:xml];relation["name" = "' + city + '"]["admin_level"="8"]; out geom;'
    # admin_level = 8 (tutto il comune)
    #query='[out:xml]; relation(41208); out geom;'
    # solo area urbana di sabaudia
    query='[out:xml]; relation(8309151); out geom;'

    if debug:
        print("Executing query: {}".format(query))

    query = url + query

    response = requests.get(query)
    string = response.text.replace("\n","")
    city_geojson = osm2geojson.xml2geojson(string)

    outfile = city + '.geojson'
    with open(outfile, 'w') as f:
        geojson.dump(city_geojson, f)
    
    if debug: 
        print("{} geojson saved to {}".format(city, outfile))

    return city_geojson


if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("Usage: {} <city name>".format(sys.argv[0]))
        sys.exit(111)
    
    city = sys.argv[1]


    get_geojson(city, True, True)



