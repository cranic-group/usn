import os

# >>>>>>>>>>>>>>>>>>>> POPULATION 
# Population age groups
GROUP_NAMES  = ['children', 'young', 'adults', 'elderly']
AGE_GROUPS   = [18, 35, 65, 100] 
GROUP2GROUP  = {'children':{'children':10.3139591, 'young':1.4095328, 'adults':3.004145, 'elderly':0.3194044}, 'young':{'children':1.1164900, 'young':5.0501872, 'adults':2.671315, 'elderly':0.5556512}, 'adults':{'children':1.4129868, 'young':1.5862171, 'adults':3.777106, 'elderly':0.8300762}, 'elderly':{'children':0.2498358, 'young':0.5487018, 'adults':1.380431, 'elderly':1.2556003}}
GROUP_FREQ   = {'children':0.15, 'young':0.25, 'adults':0.4, 'elderly':0.2}
# the path to the population file should not be modified!!! 

# This parameter is read by gen_population.sh
POPULATION_PATH = './data/population/'                                                        
# the first part of the population file, the last part is the city name
POPULATION_FILE_NAME = 'population_'
# The final dataframe with the population that live in the territory
POPULATION_DATAFRAME_FILE = 'population_dataframe.pickle'

# >>>>>>>>>>>>>>>>>>>> TERRITORY
# Tile dimensions in meters
# These parameters are read by gen_population.sh
TX = 1000
TY = 1000
# Distance between nodes in the same tile
ZERO_DIST = (TX+TY)/4

# Projection used 
INPUT_CRS    = 'EPSG:4326' #wgs84
OUTPUT_CRS   = 'EPSG:32632' #italy THIS MUST BE A FLAT PROJECTION IN METERS

# Territory files store information about the input territory in both original (wsg84) or projected coordinates
TERRITORY_PICKLE_FILE_NOT_PROJECTED = 'territory_not_projected.pickle'
TERRITORY_PICKLE_FILE_PROJECTED = 'territory_projected.pickle' 
TERRITORY_PATH = POPULATION_PATH
# File with information extracted from the input population tiff 
TIFF_INFO_FILE = 'tiff_info.pickle'
# Plot of the reconstructed grid of tiles that covers the territory
GEODATA_CETERS_PLOT_FILE = 'geodata_centers.pdf'
CITY_GEOJSON = 'city.geojson'


# >>>>>>>>>>>>>>>>>>>> WRAPPERS
# Wrappers to get the path to population and territory files from all the modules
def get_territory_file(city, projected=True):
    if projected:
        return os.path.join(TERRITORY_PATH, city, TERRITORY_PICKLE_FILE_PROJECTED)
    else:
        return os.path.join(TERRITORY_PATH, city, TERRITORY_PICKLE_FILE_NOT_PROJECTED)

def get_population_file(city):
    return os.path.join(POPULATION_PATH, city, POPULATION_FILE_NAME + city + '.csv')



# >>>>>>>>>>>>>>>>>>> MISC
# Path for python code in scripts 
DEFAULT_PATH = './'
# Not used?
RANDOM_SEED  = None
# The openstreet map overpass api url 
OVERPASS_URL = 'https://overpass-api.de/api/interpreter?data='
# Default figure size to create plot
FIGURE_SIZE = (8,8)
# Precision of the numerical solution for the block-based normalization
PRECISION = 0.0000001


# >>>>>>>>>>>>>>>>>>>> EOF


##################################################################################################
# WARNING (from extract_population_from_worldpop_tif.py)
# The extracted data is 2 pixel smaller than the given bbox, to have all the
# data points you should give a little larger input bbox. 
# tiff are expexted in wgs84, bbox must be the same.
# tiff file name are expected like: nation_sex_age_year.tiff
# WARNING (from compute_territory.py)
# the origin of tiff files is up-left while the origin of our territory is bottom-left
##################################################################################################
