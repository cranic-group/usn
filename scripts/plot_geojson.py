#
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#
#    This file is part of USN.
#
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#
#
import sys
import os
import geopandas as gp
import contextily as ctx
import  matplotlib.pylab as plt
from config import FIGURE_SIZE

plt.rcParams["figure.figsize"] = FIGURE_SIZE
plt.style.use('ggplot')

def plot_geojson(geodf, myname):
    # Convert to the same epsg as common maps (openstreet map)
    fig = plt.figure()
    geodf = geodf.to_crs(epsg=3857)
    ax = geodf.plot(figsize=(10, 10), alpha=0.5, edgecolor='k')
    ctx.add_basemap(ax, zoom = 12)
    myname = myname + '.pdf'
    plt.savefig(myname)
    print("geojson plot saved to {}".format(myname))
    return
    

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("Usage: {} <input_gejson>".format(sys.argv[0]))
        sys.exit(111)
    
    input_file = sys.argv[1]

    mydir, myname = os.path.split(input_file)

    df = gp.read_file(input_file)

    plot_geojson(df, myname)

