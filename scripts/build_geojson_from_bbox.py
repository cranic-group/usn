import sys
from shapely.geometry import box
import pandas as pd
import geopandas as gpd
import contextily as ctx
import  matplotlib.pylab as plt
import pickle

DEFAULT_CRS  = 'EPSG:4326' #wgs84
BBOX_PLOT_GEOJSON_FILE = "bbox.pdf"
BBOX_GEJSON_FILE = 'bbox.geojson'


def bbox_to_geojson(bbox):
    minx, miny, maxx, maxy = bbox
    bbox = box(minx, miny, maxx, maxy)
    b = gpd.GeoSeries(bbox, crs=DEFAULT_CRS)
    return b


if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("Usage: {} 'minx, miny, maxx, maxy' ".format(sys.argv[0]))
        sys.exit(111)

    bbox = tuple(float(x) for x in sys.argv[1].split(','))
    print("input bbox: {}".format(bbox))

    b = bbox_to_geojson(bbox)
    b.to_file(BBOX_GEJSON_FILE, driver='GeoJSON')
    print("bbox geojson saved to: {}".format(BBOX_GEJSON_FILE))

    # convert to CTX MAP CRS
    b = b.to_crs(epsg=3857)
    # add bbox in green
    ax = b.plot(alpha=0.5, edgecolor='g')
    ctx.add_basemap(ax, zoom = 12)
    plt.savefig(BBOX_PLOT_GEOJSON_FILE)
    print("Bbox, saved to {}".format(BBOX_PLOT_GEOJSON_FILE))



