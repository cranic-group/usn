TILE   = (500,500)
# Ntiles is used only to define the raidus! Below is recomputed as radius / tile
NTILES = (5,5)
GENTYPE = "radial_increasing" # can be radial or uniform 
SEED    = 1
CITY_NAME = f'idealcity_{GENTYPE}'
SAMPLE_SIZE = 10000
