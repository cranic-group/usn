import sys
import geopandas as gp
from shapely.geometry import box
import pyproj
from pyproj import CRS
from pyproj import Transformer

# From wgs84 to wgs84_32n
# wgs84_32n is a projection on a flat surface in meters of a slice in central europe
# coord = (lat, lon)
def project_coord(coord, inCRS, outCRS):
    inproj     = pyproj.CRS(inCRS)
    outproj    = pyproj.CRS(outCRS)

    transf = Transformer.from_crs(inproj, outproj)
    a = transf.transform(coord[1], coord[0])
    return a


def get_info_for_territory(input_geojson, tile_dim_x, tile_dim_y, output_CRS, debug=False):

    df = gp.read_file(input_geojson)
    origin_WGS84 = df.bounds['miny'][0], df.bounds['minx'][0]

    df = df.to_crs(output_CRS)

    boundary = df.bounds
    minx, miny, maxx, maxy = df.bounds['minx'][0], df.bounds['miny'][0], df.bounds['maxx'][0], df.bounds['maxy'][0]
    xlen = abs(minx - maxx)
    ylen = abs(miny - maxy)

    ntile_x = xlen/tile_dim_x
    ntile_y = ylen/tile_dim_y

    bbox = box(*df.total_bounds)
    cx, cy = bbox.centroid.x, bbox.centroid.y
    bbox_center = (cy, cx)
    x, y = bbox.exterior.coords.xy
    bbox_vertices = list(zip(y, x))

    origin = bbox_vertices[3]

    if debug:
        print("")
        print("="*80)
        print("boundary: ")
        print(boundary)
        print("vertices:     {}".format(bbox_vertices))
        print("center:       {}".format(bbox_center))
        print("origin:       {}".format(origin))
        print("origin_WGS84: {}".format(origin_WGS84))
        print("xlen, ylen:   {}, {}".format(xlen, ylen))
        print("xtile, ytile: {}, {}".format(tile_dim_x, tile_dim_y))
        print("ntx, nty:     {}, {}".format(ntile_x, ntile_y))
        print("="*80)
        print("")

    return ntile_x, ntile_y, boundary, bbox_center, bbox_vertices




# Calcolo del numero di tile necessari per copire una città
# Prendo il bbox che contiene lo shape della citta in wgs84, lo proietto UTM_32N,
# misuro altezza e larghezza, la divido per la dimensione del tile e approssimo per
# difetto.
if __name__ == '__main__':
    
    if len(sys.argv) < 4:
        print("Usage: {} <input_gejson> <tile dimension x> <tile dimension y> ".format(sys.argv[0]))
        sys.exit(111)
    
    input_file = sys.argv[1]
    tile_dim_x = float(sys.argv[2])
    tile_dim_y = float(sys.argv[3])
    output_CRS = 'EPSG:32632'

    get_info_for_territory(input_file, tile_dim_x, tile_dim_y, output_CRS, debug=True)

    

