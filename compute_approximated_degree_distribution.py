from config.config import GROUP2GROUP 
from create_social_base import get_input_data_structures, get_population_dictionary, get_grid_centers, compute_grid_distances, preprocess_g2g_matrix, preprocess_distances, count_tot_nodes_per_block, compute_block2block_matrix 
from lib.libfunc import read_from_config
from datetime import datetime
import numpy as np
import igraph
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from collections import Counter
from scipy.stats import lognorm
import pickle
import sys
import os

in_path = ''
config_file = 'config/config_ideal.ini'
city = read_from_config(config_file, 'CITY', 'city') 
# gname = f'idealcity_runs/{city}_approximation_7/sb_graph.pickle'
gname = f'idealcity_runs/test_city_approximation/sb_graph.pickle'
print(f'reading graph from {gname}')
g = igraph.read(gname)

data      = get_input_data_structures(in_path, config_file)
territory = data['territory']
pop_info  = data['pop_info']
print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Selected city: {pop_info['city']}")
# Create POP
pop = {}
pop['tile_id'] = np.asarray(g.vs['tile_id'])
pop['type'] = np.asarray(g.vs['type'])
pop['fitness'] = np.asarray(g.vs['fitness'])
ngroups = len(pop_info['group_freqs'])
pop['block'] = ngroups*pop['tile_id']+pop['type']
dist_info = data['dist_info']
mu        = read_from_config(config_file, 'SOCIALITY', 'avg_deg')
g2g_type  = read_from_config(config_file, 'SOCIALITY', 'group2group_type')
if g2g_type == 'constant':
    group_names = pop_info['group_names']
    n = len(group_names)
    g2g_matrix = np.ones((n,n))
else:
    #g2g_dict = read_from_config(config_file, 'SOCIALITY', 'group2group')
    g2g_dict = GROUP2GROUP
    g2g_matrix = preprocess_g2g_matrix(g2g_dict, pop_info, pop)
# Compute the coordinates of the grid centers
grid_centers = get_grid_centers(territory)
# Compute tile_0 distance array: grid_distance_array_0
grid_distance_array_0 = compute_grid_distances(0, grid_centers, dist_info)
# Preprocess grid distances
preprocessed_grid_distance_array_0 = preprocess_distances(grid_distance_array_0, dist_info)
nodes_per_block = count_tot_nodes_per_block(pop)
ntiles  = len(grid_distance_array_0)
block_size_array = np.array([nodes_per_block.get((t,a),0) for t in range(ntiles) for a in range(ngroups)])
# Normalize probability coefficients; normalization is computed -per block- so we use a (nblocks,nblocks) array H
L = compute_block2block_matrix(mu, pop, block_size_array, g2g_matrix, preprocessed_grid_distance_array_0, territory) 

with open('test_L_2.pickle', 'wb') as f_:
    pickle.dump(L,f_)

fitness_func = pop_info['fitness_func']
if fitness_func == 'lognormal':
    b=0.5
    loc=1
    scale=2
    r = lognorm(b, loc=loc, scale=scale)
    avg_fitness = r.mean()
else:
    print('error in fitness_func')
    sys.exit()

# for i in range(10):
# i = 7
# gname = f'idealcity_runs/{city}_exact_{i}/sb_graph.pickle'
# print(f'reading graph from {gname}')
# g = igraph.read(gname)
N = g.vcount()
nblocks = max(pop['block'])+1
# print(L[np.triu_indices(nblocks)].sum()*2/N)
# K = 2*np.sqrt(2)/N * sum([(0.5*L[b].sum()+L[b,b])/(block_size_array[b]-1) for b in range(nblocks)])
# K = 2/N * sum([(0.5*L[b].sum()+0.5*L[b,b]) for b in range(nblocks)])
# print(K)
# K = 2/N * sum([L[b].sum() for b in range(nblocks)])

sums = np.zeros(nblocks)
prods = np.zeros((nblocks,nblocks))
for b1 in range(nblocks):
    in_b1 = pop['block']==b1
    f1 = pop['fitness'][in_b1]
    sums[b1] = f1.sum()
    for b2 in range(nblocks):
        if b1==b2:
            l = in_b1.sum()
            idxs = np.triu_indices(l,1)
            prods[b1,b1] = (f1[idxs[0]]*f1[idxs[1]]).sum()
        else:
            in_b2 = pop['block']==b2
            f2 = pop['fitness'][in_b2]
            prods[b1,b2] = np.outer(f1,f2).sum()
            prods[b2,b1] = prods[b1,b2]

g = g.subgraph_edges([e.tuple for e in g.es if 'friendship' in e['layer']], delete_vertices=False)
blocks = ngroups*np.array(g.vs['tile_id'])+np.array(g.vs['type'])

########################################################################################

K15 = [[],[]]
K17 = []
for b in range(nblocks):
    K15[0].append(L[b,b]/prods[b,b])
    K15[1].append((L[b].sum()-L[b,b])/sums[b])
    K17.append(2/block_size_array[b] * (0.5*L[b].sum()+0.5*L[b,b]))


with open('test_data.pickle', 'wb') as f_:
    pickle.dump((K15,sums),f_)


ks15 = []
ks17 = []
for v in g.vs:
    x = v['fitness']
    try:
        b = v['block']
    except:
        b = ngroups*v['tile_id']+v['type']
    k = x*(K15[0][b]*(sums[b]-x)+K15[1][b])
    ks15.append(k)
    k = x/avg_fitness * K17[b]
    ks17.append(k)

print('15:', np.min(ks15), np.max(ks15), np.mean(ks15))
print('17:', np.min(ks17), np.max(ks17), np.mean(ks17))

d = {'degree':ks15+ks17, 'what':['pred15']*N+['pred17']*N}
d['degree'].extend(g.degree())
d['what'].extend(['emp']*N)
# d['degree'].extend([x*mu/avg_fitness for x in g.vs['fitness']])
# d['what'].extend(['pred18']*N)
ds = g.degree()
print(np.min(ds), np.max(ds), np.mean(ds))

kmax = max(g.degree())
y = np.zeros(kmax+1)
x = np.arange(kmax+1)
for b in range(nblocks):
    const = avg_fitness*block_size_array[b]/(L[b].sum()+L[b,b])
    _x = x*const
    y = y+block_size_array[b]/N*r.pdf(_x)*const

# data_df = pd.read_pickle('idealcities_degree_df.pickle')
# for i in range(10):
#     emp_ks = data_df[data_df['run']==i][data_df['city']=='uniform'][data_df['solver']=='approximation']['degree'].tolist()
#     # print(f'emp {i}:', np.min(emp_ks), np.max(emp_ks), np.mean(emp_ks))
#     # print(Counter(emp_ks)[0])
#     d['degree'].extend(emp_ks)
#     d['what'].extend([f'emp{i}']*N)

# plot
# sns.displot(data=df, x="degree", hue="solver", col='city', kind='kde', fill=False)
# plot_name = 'idealcity_solver_degree_compare_cities_with_theory.png'
# plt.ylabel('density')
# plt.gcf().set_size_inches(8,4)
# plt.savefig(plot_name, bbox_inches='tight')
# print(f'plot saved at {plot_name}')
sns.displot(data=d, x="degree", hue='what', common_norm=False, kind='kde', fill=False)
# sns.lineplot(data=d, x="degree", y='count', hue='what') #, fill=False)
plt.plot(x, y)
plot_name = 'test_degree_dist.png'
plt.ylabel('density')
plt.gcf().set_size_inches(8,4)
# plt.xscale('log')
# plt.yscale('log')
plt.savefig(plot_name, bbox_inches='tight')
print(f'plot saved at {plot_name}')

plt.clf()
d = {'degree diff':[i-j for i,j in zip(ks15,g.degree())]}
sns.displot(data=d, x="degree diff", kind='kde', fill=False)
# sns.lineplot(data=d, x="degree", y='count', hue='what') #, fill=False)
plot_name = 'test_degree_diff_dist.png'
plt.ylabel('density')
plt.gcf().set_size_inches(8,4)
# plt.xscale('log')
# plt.yscale('log')
plt.savefig(plot_name, bbox_inches='tight')
print(f'plot saved at {plot_name}')
plt.clf()
sns.scatterplot(x=range(g.vcount()), y=d['degree diff'])
plot_name = 'test_degree_diff.png'
plt.ylabel('degree difference (pred-emp)')
plt.gcf().set_size_inches(8,4)
plt.savefig(plot_name, bbox_inches='tight')
print(f'plot saved at {plot_name}')

degrees = g.degree()
d = {'degree diff':[], 'age':[]}
for i in range(4):
    y = [ks15[j]-degrees[j] for j in range(g.vcount()) if g.vs[j]['type']==i]
    d['degree diff'].extend(y)
    d['age'].extend([i]*len(y))
    plt.clf()
    sns.scatterplot(x=range(len(y)), y=y)
    plot_name = f'test_degree_diff_age_{i}.png'
    plt.ylabel('degree difference (pred-emp)')
    plt.gcf().set_size_inches(8,4)
    plt.savefig(plot_name, bbox_inches='tight')
    print(f'plot saved at {plot_name}')
plt.clf()
sns.displot(data=d, x="degree diff", hue='age', kind='kde', fill=False)
# sns.lineplot(data=d, x="degree", y='count', hue='what') #, fill=False)
plot_name = 'test_degree_diff_dist_age.png'
plt.ylabel('density')
plt.gcf().set_size_inches(8,4)
# plt.xscale('log')
# plt.yscale('log')
plt.savefig(plot_name, bbox_inches='tight')
print(f'plot saved at {plot_name}')

sys.exit()

####################################################################################################################

L_emp = np.zeros((nblocks,nblocks))
if not os.path.exists('test_plots'):
    os.makedirs('test_plots')

for e in g.es:
    b1 = blocks[e.source]
    b2 = blocks[e.target]
    L_emp[min(b1,b2),max(b1,b2)] += 1
    if b1!=b2:
        L_emp[max(b1,b2),min(b1,b2)] += 1


with open('test_L_3.pickle', 'wb') as f_:
    pickle.dump(L_emp,f_)

sys.exit()

#######################################################################################

ks15 = {b1:{b2:[] for b2 in range(nblocks)} for b1 in range(nblocks)}
# ks17 = {b1:{b2:[] for b2 in range(nblocks)} for b1 in range(nblocks)}
for v in g.vs:
    x = v['fitness']
    b1 = ngroups*v['tile_id']+v['type']
    for b2 in range(nblocks):
        if b1==b2:
            ks15[b1][b1].append(L[b1,b1]/prods[b1,b1]*x*(sums[b1]-x))
        else:
            ks15[b1][b2].append(L[b1,b2]/sums[b1]*x)


g = g.subgraph_edges([e.tuple for e in g.es if 'friendship' in e['layer']], delete_vertices=False)
blocks = ngroups*np.array(g.vs['tile_id'])+np.array(g.vs['type'])

if not os.path.exists('test_plots'):
    os.makedirs('test_plots')
for b1 in range(nblocks):
    for b2 in range(nblocks):
        plt.clf()
        h = g.subgraph_edges([e.tuple for e in g.es if (blocks[e.source]==b1 and blocks[e.target]==b2) or (blocks[e.source]==b2 and blocks[e.target]==b1)], delete_vertices=False)
        d = {'degree':ks15[b1][b2]+h.degree(), 'what':['pred15']*len(ks15[b1][b2])+['emp']*h.vcount()}
        sns.displot(data=d, x="degree", hue='what', kind='kde', fill=False)
        plot_name = f'test_plots/test_degree_dist_{b1}_{b2}.png'
        plt.ylabel('density')
        plt.gcf().set_size_inches(8,4)
        # plt.xscale('log')
        # plt.yscale('log')
        plt.savefig(plot_name, bbox_inches='tight')
        print(f'plot saved at {plot_name}')
    sys.exit()



#######################################################################################



